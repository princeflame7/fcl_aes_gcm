//forward butterfly
//outA = inA+inB*inC
//outB = inA+inB*(inC+1)

module gf256_bf_0x00 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0];
assign outB[0] = inA[0] ^ inB[0];
assign outA[1] = inA[1];
assign outB[1] = inA[1] ^ inB[1];
assign outA[2] = inA[2];
assign outB[2] = inA[2] ^ inB[2];
assign outA[3] = inA[3];
assign outB[3] = inA[3] ^ inB[3];
assign outA[4] = inA[4];
assign outB[4] = inA[4] ^ inB[4];
assign outA[5] = inA[5];
assign outB[5] = inA[5] ^ inB[5];
assign outA[6] = inA[6];
assign outB[6] = inA[6] ^ inB[6];
assign outA[7] = inA[7];
assign outB[7] = inA[7] ^ inB[7];

endmodule

module gf256_bf_0x01 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[0];
assign outB[0] = inA[0];
assign outA[1] = inA[1] ^ inB[1];
assign outB[1] = inA[1];
assign outA[2] = inA[2] ^ inB[2];
assign outB[2] = inA[2];
assign outA[3] = inA[3] ^ inB[3];
assign outB[3] = inA[3];
assign outA[4] = inA[4] ^ inB[4];
assign outB[4] = inA[4];
assign outA[5] = inA[5] ^ inB[5];
assign outB[5] = inA[5];
assign outA[6] = inA[6] ^ inB[6];
assign outB[6] = inA[6];
assign outA[7] = inA[7] ^ inB[7];
assign outB[7] = inA[7];

endmodule

module gf256_bf_0x02 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[1];
assign outB[0] = inA[0] ^ inB[0] ^ inB[1];
assign outA[1] = inA[1] ^ inB[0] ^ inB[1];
assign outB[1] = inA[1] ^ inB[0];
assign outA[2] = inA[2] ^ inB[3];
assign outB[2] = inA[2] ^ inB[2] ^ inB[3];
assign outA[3] = inA[3] ^ inB[2] ^ inB[3];
assign outB[3] = inA[3] ^ inB[2];
assign outA[4] = inA[4] ^ inB[5];
assign outB[4] = inA[4] ^ inB[4] ^ inB[5];
assign outA[5] = inA[5] ^ inB[4] ^ inB[5];
assign outB[5] = inA[5] ^ inB[4];
assign outA[6] = inA[6] ^ inB[7];
assign outB[6] = inA[6] ^ inB[6] ^ inB[7];
assign outA[7] = inA[7] ^ inB[6] ^ inB[7];
assign outB[7] = inA[7] ^ inB[6];

endmodule

module gf256_bf_0x03 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[0] ^ inB[1];
assign outB[0] = inA[0] ^ inB[1];
assign outA[1] = inA[1] ^ inB[0];
assign outB[1] = inA[1] ^ inB[0] ^ inB[1];
assign outA[2] = inA[2] ^ inB[2] ^ inB[3];
assign outB[2] = inA[2] ^ inB[3];
assign outA[3] = inA[3] ^ inB[2];
assign outB[3] = inA[3] ^ inB[2] ^ inB[3];
assign outA[4] = inA[4] ^ inB[4] ^ inB[5];
assign outB[4] = inA[4] ^ inB[5];
assign outA[5] = inA[5] ^ inB[4];
assign outB[5] = inA[5] ^ inB[4] ^ inB[5];
assign outA[6] = inA[6] ^ inB[6] ^ inB[7];
assign outB[6] = inA[6] ^ inB[7];
assign outA[7] = inA[7] ^ inB[6];
assign outB[7] = inA[7] ^ inB[6] ^ inB[7];

endmodule

module gf256_bf_0x04 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[3];
assign outB[0] = inA[0] ^ inB[0] ^ inB[3];
assign outA[1] = inA[1] ^ inB[2] ^ inB[3];
assign outB[1] = inA[1] ^ inB[1] ^ inB[2] ^ inB[3];
assign outA[2] = inA[2] ^ inB[0] ^ inB[2];
assign outB[2] = inA[2] ^ inB[0];
assign outA[3] = inA[3] ^ inB[1] ^ inB[3];
assign outB[3] = inA[3] ^ inB[1];
assign outA[4] = inA[4] ^ inB[7];
assign outB[4] = inA[4] ^ inB[4] ^ inB[7];
assign outA[5] = inA[5] ^ inB[6] ^ inB[7];
assign outB[5] = inA[5] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[6] = inA[6] ^ inB[4] ^ inB[6];
assign outB[6] = inA[6] ^ inB[4];
assign outA[7] = inA[7] ^ inB[5] ^ inB[7];
assign outB[7] = inA[7] ^ inB[5];

endmodule

module gf256_bf_0x05 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[0] ^ inB[3];
assign outB[0] = inA[0] ^ inB[3];
assign outA[1] = inA[1] ^ inB[1] ^ inB[2] ^ inB[3];
assign outB[1] = inA[1] ^ inB[2] ^ inB[3];
assign outA[2] = inA[2] ^ inB[0];
assign outB[2] = inA[2] ^ inB[0] ^ inB[2];
assign outA[3] = inA[3] ^ inB[1];
assign outB[3] = inA[3] ^ inB[1] ^ inB[3];
assign outA[4] = inA[4] ^ inB[4] ^ inB[7];
assign outB[4] = inA[4] ^ inB[7];
assign outA[5] = inA[5] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[5] = inA[5] ^ inB[6] ^ inB[7];
assign outA[6] = inA[6] ^ inB[4];
assign outB[6] = inA[6] ^ inB[4] ^ inB[6];
assign outA[7] = inA[7] ^ inB[5];
assign outB[7] = inA[7] ^ inB[5] ^ inB[7];

endmodule

module gf256_bf_0x06 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[1] ^ inB[3];
assign outB[0] = inA[0] ^ inB[0] ^ inB[1] ^ inB[3];
assign outA[1] = inA[1] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[3];
assign outB[1] = inA[1] ^ inB[0] ^ inB[2] ^ inB[3];
assign outA[2] = inA[2] ^ inB[0] ^ inB[2] ^ inB[3];
assign outB[2] = inA[2] ^ inB[0] ^ inB[3];
assign outA[3] = inA[3] ^ inB[1] ^ inB[2];
assign outB[3] = inA[3] ^ inB[1] ^ inB[2] ^ inB[3];
assign outA[4] = inA[4] ^ inB[5] ^ inB[7];
assign outB[4] = inA[4] ^ inB[4] ^ inB[5] ^ inB[7];
assign outA[5] = inA[5] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[5] = inA[5] ^ inB[4] ^ inB[6] ^ inB[7];
assign outA[6] = inA[6] ^ inB[4] ^ inB[6] ^ inB[7];
assign outB[6] = inA[6] ^ inB[4] ^ inB[7];
assign outA[7] = inA[7] ^ inB[5] ^ inB[6];
assign outB[7] = inA[7] ^ inB[5] ^ inB[6] ^ inB[7];

endmodule

module gf256_bf_0x07 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[0] ^ inB[1] ^ inB[3];
assign outB[0] = inA[0] ^ inB[1] ^ inB[3];
assign outA[1] = inA[1] ^ inB[0] ^ inB[2] ^ inB[3];
assign outB[1] = inA[1] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[3];
assign outA[2] = inA[2] ^ inB[0] ^ inB[3];
assign outB[2] = inA[2] ^ inB[0] ^ inB[2] ^ inB[3];
assign outA[3] = inA[3] ^ inB[1] ^ inB[2] ^ inB[3];
assign outB[3] = inA[3] ^ inB[1] ^ inB[2];
assign outA[4] = inA[4] ^ inB[4] ^ inB[5] ^ inB[7];
assign outB[4] = inA[4] ^ inB[5] ^ inB[7];
assign outA[5] = inA[5] ^ inB[4] ^ inB[6] ^ inB[7];
assign outB[5] = inA[5] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[6] = inA[6] ^ inB[4] ^ inB[7];
assign outB[6] = inA[6] ^ inB[4] ^ inB[6] ^ inB[7];
assign outA[7] = inA[7] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[7] = inA[7] ^ inB[5] ^ inB[6];

endmodule

module gf256_bf_0x08 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[2] ^ inB[3];
assign outB[0] = inA[0] ^ inB[0] ^ inB[2] ^ inB[3];
assign outA[1] = inA[1] ^ inB[2];
assign outB[1] = inA[1] ^ inB[1] ^ inB[2];
assign outA[2] = inA[2] ^ inB[1] ^ inB[3];
assign outB[2] = inA[2] ^ inB[1] ^ inB[2] ^ inB[3];
assign outA[3] = inA[3] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[3];
assign outB[3] = inA[3] ^ inB[0] ^ inB[1] ^ inB[2];
assign outA[4] = inA[4] ^ inB[6] ^ inB[7];
assign outB[4] = inA[4] ^ inB[4] ^ inB[6] ^ inB[7];
assign outA[5] = inA[5] ^ inB[6];
assign outB[5] = inA[5] ^ inB[5] ^ inB[6];
assign outA[6] = inA[6] ^ inB[5] ^ inB[7];
assign outB[6] = inA[6] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[7] = inA[7] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[7] = inA[7] ^ inB[4] ^ inB[5] ^ inB[6];

endmodule

module gf256_bf_0x09 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[0] ^ inB[2] ^ inB[3];
assign outB[0] = inA[0] ^ inB[2] ^ inB[3];
assign outA[1] = inA[1] ^ inB[1] ^ inB[2];
assign outB[1] = inA[1] ^ inB[2];
assign outA[2] = inA[2] ^ inB[1] ^ inB[2] ^ inB[3];
assign outB[2] = inA[2] ^ inB[1] ^ inB[3];
assign outA[3] = inA[3] ^ inB[0] ^ inB[1] ^ inB[2];
assign outB[3] = inA[3] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[3];
assign outA[4] = inA[4] ^ inB[4] ^ inB[6] ^ inB[7];
assign outB[4] = inA[4] ^ inB[6] ^ inB[7];
assign outA[5] = inA[5] ^ inB[5] ^ inB[6];
assign outB[5] = inA[5] ^ inB[6];
assign outA[6] = inA[6] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[6] = inA[6] ^ inB[5] ^ inB[7];
assign outA[7] = inA[7] ^ inB[4] ^ inB[5] ^ inB[6];
assign outB[7] = inA[7] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];

endmodule

module gf256_bf_0x0a (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[1] ^ inB[2] ^ inB[3];
assign outB[0] = inA[0] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[3];
assign outA[1] = inA[1] ^ inB[0] ^ inB[1] ^ inB[2];
assign outB[1] = inA[1] ^ inB[0] ^ inB[2];
assign outA[2] = inA[2] ^ inB[1];
assign outB[2] = inA[2] ^ inB[1] ^ inB[2];
assign outA[3] = inA[3] ^ inB[0] ^ inB[1];
assign outB[3] = inA[3] ^ inB[0] ^ inB[1] ^ inB[3];
assign outA[4] = inA[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[4] = inA[4] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[5] = inA[5] ^ inB[4] ^ inB[5] ^ inB[6];
assign outB[5] = inA[5] ^ inB[4] ^ inB[6];
assign outA[6] = inA[6] ^ inB[5];
assign outB[6] = inA[6] ^ inB[5] ^ inB[6];
assign outA[7] = inA[7] ^ inB[4] ^ inB[5];
assign outB[7] = inA[7] ^ inB[4] ^ inB[5] ^ inB[7];

endmodule

module gf256_bf_0x0b (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[3];
assign outB[0] = inA[0] ^ inB[1] ^ inB[2] ^ inB[3];
assign outA[1] = inA[1] ^ inB[0] ^ inB[2];
assign outB[1] = inA[1] ^ inB[0] ^ inB[1] ^ inB[2];
assign outA[2] = inA[2] ^ inB[1] ^ inB[2];
assign outB[2] = inA[2] ^ inB[1];
assign outA[3] = inA[3] ^ inB[0] ^ inB[1] ^ inB[3];
assign outB[3] = inA[3] ^ inB[0] ^ inB[1];
assign outA[4] = inA[4] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[4] = inA[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[5] = inA[5] ^ inB[4] ^ inB[6];
assign outB[5] = inA[5] ^ inB[4] ^ inB[5] ^ inB[6];
assign outA[6] = inA[6] ^ inB[5] ^ inB[6];
assign outB[6] = inA[6] ^ inB[5];
assign outA[7] = inA[7] ^ inB[4] ^ inB[5] ^ inB[7];
assign outB[7] = inA[7] ^ inB[4] ^ inB[5];

endmodule

module gf256_bf_0x0c (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[2];
assign outB[0] = inA[0] ^ inB[0] ^ inB[2];
assign outA[1] = inA[1] ^ inB[3];
assign outB[1] = inA[1] ^ inB[1] ^ inB[3];
assign outA[2] = inA[2] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[3];
assign outB[2] = inA[2] ^ inB[0] ^ inB[1] ^ inB[3];
assign outA[3] = inA[3] ^ inB[0] ^ inB[2];
assign outB[3] = inA[3] ^ inB[0] ^ inB[2] ^ inB[3];
assign outA[4] = inA[4] ^ inB[6];
assign outB[4] = inA[4] ^ inB[4] ^ inB[6];
assign outA[5] = inA[5] ^ inB[7];
assign outB[5] = inA[5] ^ inB[5] ^ inB[7];
assign outA[6] = inA[6] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[6] = inA[6] ^ inB[4] ^ inB[5] ^ inB[7];
assign outA[7] = inA[7] ^ inB[4] ^ inB[6];
assign outB[7] = inA[7] ^ inB[4] ^ inB[6] ^ inB[7];

endmodule

module gf256_bf_0x0d (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[0] ^ inB[2];
assign outB[0] = inA[0] ^ inB[2];
assign outA[1] = inA[1] ^ inB[1] ^ inB[3];
assign outB[1] = inA[1] ^ inB[3];
assign outA[2] = inA[2] ^ inB[0] ^ inB[1] ^ inB[3];
assign outB[2] = inA[2] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[3];
assign outA[3] = inA[3] ^ inB[0] ^ inB[2] ^ inB[3];
assign outB[3] = inA[3] ^ inB[0] ^ inB[2];
assign outA[4] = inA[4] ^ inB[4] ^ inB[6];
assign outB[4] = inA[4] ^ inB[6];
assign outA[5] = inA[5] ^ inB[5] ^ inB[7];
assign outB[5] = inA[5] ^ inB[7];
assign outA[6] = inA[6] ^ inB[4] ^ inB[5] ^ inB[7];
assign outB[6] = inA[6] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[7] = inA[7] ^ inB[4] ^ inB[6] ^ inB[7];
assign outB[7] = inA[7] ^ inB[4] ^ inB[6];

endmodule

module gf256_bf_0x0e (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[1] ^ inB[2];
assign outB[0] = inA[0] ^ inB[0] ^ inB[1] ^ inB[2];
assign outA[1] = inA[1] ^ inB[0] ^ inB[1] ^ inB[3];
assign outB[1] = inA[1] ^ inB[0] ^ inB[3];
assign outA[2] = inA[2] ^ inB[0] ^ inB[1] ^ inB[2];
assign outB[2] = inA[2] ^ inB[0] ^ inB[1];
assign outA[3] = inA[3] ^ inB[0] ^ inB[3];
assign outB[3] = inA[3] ^ inB[0];
assign outA[4] = inA[4] ^ inB[5] ^ inB[6];
assign outB[4] = inA[4] ^ inB[4] ^ inB[5] ^ inB[6];
assign outA[5] = inA[5] ^ inB[4] ^ inB[5] ^ inB[7];
assign outB[5] = inA[5] ^ inB[4] ^ inB[7];
assign outA[6] = inA[6] ^ inB[4] ^ inB[5] ^ inB[6];
assign outB[6] = inA[6] ^ inB[4] ^ inB[5];
assign outA[7] = inA[7] ^ inB[4] ^ inB[7];
assign outB[7] = inA[7] ^ inB[4];

endmodule

module gf256_bf_0x0f (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[0] ^ inB[1] ^ inB[2];
assign outB[0] = inA[0] ^ inB[1] ^ inB[2];
assign outA[1] = inA[1] ^ inB[0] ^ inB[3];
assign outB[1] = inA[1] ^ inB[0] ^ inB[1] ^ inB[3];
assign outA[2] = inA[2] ^ inB[0] ^ inB[1];
assign outB[2] = inA[2] ^ inB[0] ^ inB[1] ^ inB[2];
assign outA[3] = inA[3] ^ inB[0];
assign outB[3] = inA[3] ^ inB[0] ^ inB[3];
assign outA[4] = inA[4] ^ inB[4] ^ inB[5] ^ inB[6];
assign outB[4] = inA[4] ^ inB[5] ^ inB[6];
assign outA[5] = inA[5] ^ inB[4] ^ inB[7];
assign outB[5] = inA[5] ^ inB[4] ^ inB[5] ^ inB[7];
assign outA[6] = inA[6] ^ inB[4] ^ inB[5];
assign outB[6] = inA[6] ^ inB[4] ^ inB[5] ^ inB[6];
assign outA[7] = inA[7] ^ inB[4];
assign outB[7] = inA[7] ^ inB[4] ^ inB[7];

endmodule

module gf256_bf_0x10 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[6] ^ inB[7];
assign outB[0] = inA[0] ^ inB[0] ^ inB[6] ^ inB[7];
assign outA[1] = inA[1] ^ inB[6];
assign outB[1] = inA[1] ^ inB[1] ^ inB[6];
assign outA[2] = inA[2] ^ inB[5] ^ inB[7];
assign outB[2] = inA[2] ^ inB[2] ^ inB[5] ^ inB[7];
assign outA[3] = inA[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[3] = inA[3] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[4] = inA[4] ^ inB[0] ^ inB[4];
assign outB[4] = inA[4] ^ inB[0];
assign outA[5] = inA[5] ^ inB[1] ^ inB[5];
assign outB[5] = inA[5] ^ inB[1];
assign outA[6] = inA[6] ^ inB[2] ^ inB[6];
assign outB[6] = inA[6] ^ inB[2];
assign outA[7] = inA[7] ^ inB[3] ^ inB[7];
assign outB[7] = inA[7] ^ inB[3];

endmodule

module gf256_bf_0x11 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[0] ^ inB[6] ^ inB[7];
assign outB[0] = inA[0] ^ inB[6] ^ inB[7];
assign outA[1] = inA[1] ^ inB[1] ^ inB[6];
assign outB[1] = inA[1] ^ inB[6];
assign outA[2] = inA[2] ^ inB[2] ^ inB[5] ^ inB[7];
assign outB[2] = inA[2] ^ inB[5] ^ inB[7];
assign outA[3] = inA[3] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[3] = inA[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[4] = inA[4] ^ inB[0];
assign outB[4] = inA[4] ^ inB[0] ^ inB[4];
assign outA[5] = inA[5] ^ inB[1];
assign outB[5] = inA[5] ^ inB[1] ^ inB[5];
assign outA[6] = inA[6] ^ inB[2];
assign outB[6] = inA[6] ^ inB[2] ^ inB[6];
assign outA[7] = inA[7] ^ inB[3];
assign outB[7] = inA[7] ^ inB[3] ^ inB[7];

endmodule

module gf256_bf_0x12 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[1] ^ inB[6] ^ inB[7];
assign outB[0] = inA[0] ^ inB[0] ^ inB[1] ^ inB[6] ^ inB[7];
assign outA[1] = inA[1] ^ inB[0] ^ inB[1] ^ inB[6];
assign outB[1] = inA[1] ^ inB[0] ^ inB[6];
assign outA[2] = inA[2] ^ inB[3] ^ inB[5] ^ inB[7];
assign outB[2] = inA[2] ^ inB[2] ^ inB[3] ^ inB[5] ^ inB[7];
assign outA[3] = inA[3] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[3] = inA[3] ^ inB[2] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[4] = inA[4] ^ inB[0] ^ inB[4] ^ inB[5];
assign outB[4] = inA[4] ^ inB[0] ^ inB[5];
assign outA[5] = inA[5] ^ inB[1] ^ inB[4];
assign outB[5] = inA[5] ^ inB[1] ^ inB[4] ^ inB[5];
assign outA[6] = inA[6] ^ inB[2] ^ inB[6] ^ inB[7];
assign outB[6] = inA[6] ^ inB[2] ^ inB[7];
assign outA[7] = inA[7] ^ inB[3] ^ inB[6];
assign outB[7] = inA[7] ^ inB[3] ^ inB[6] ^ inB[7];

endmodule

module gf256_bf_0x13 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[0] ^ inB[1] ^ inB[6] ^ inB[7];
assign outB[0] = inA[0] ^ inB[1] ^ inB[6] ^ inB[7];
assign outA[1] = inA[1] ^ inB[0] ^ inB[6];
assign outB[1] = inA[1] ^ inB[0] ^ inB[1] ^ inB[6];
assign outA[2] = inA[2] ^ inB[2] ^ inB[3] ^ inB[5] ^ inB[7];
assign outB[2] = inA[2] ^ inB[3] ^ inB[5] ^ inB[7];
assign outA[3] = inA[3] ^ inB[2] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[3] = inA[3] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[4] = inA[4] ^ inB[0] ^ inB[5];
assign outB[4] = inA[4] ^ inB[0] ^ inB[4] ^ inB[5];
assign outA[5] = inA[5] ^ inB[1] ^ inB[4] ^ inB[5];
assign outB[5] = inA[5] ^ inB[1] ^ inB[4];
assign outA[6] = inA[6] ^ inB[2] ^ inB[7];
assign outB[6] = inA[6] ^ inB[2] ^ inB[6] ^ inB[7];
assign outA[7] = inA[7] ^ inB[3] ^ inB[6] ^ inB[7];
assign outB[7] = inA[7] ^ inB[3] ^ inB[6];

endmodule

module gf256_bf_0x14 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[3] ^ inB[6] ^ inB[7];
assign outB[0] = inA[0] ^ inB[0] ^ inB[3] ^ inB[6] ^ inB[7];
assign outA[1] = inA[1] ^ inB[2] ^ inB[3] ^ inB[6];
assign outB[1] = inA[1] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[6];
assign outA[2] = inA[2] ^ inB[0] ^ inB[2] ^ inB[5] ^ inB[7];
assign outB[2] = inA[2] ^ inB[0] ^ inB[5] ^ inB[7];
assign outA[3] = inA[3] ^ inB[1] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[3] = inA[3] ^ inB[1] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[4] = inA[4] ^ inB[0] ^ inB[4] ^ inB[7];
assign outB[4] = inA[4] ^ inB[0] ^ inB[7];
assign outA[5] = inA[5] ^ inB[1] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[5] = inA[5] ^ inB[1] ^ inB[6] ^ inB[7];
assign outA[6] = inA[6] ^ inB[2] ^ inB[4];
assign outB[6] = inA[6] ^ inB[2] ^ inB[4] ^ inB[6];
assign outA[7] = inA[7] ^ inB[3] ^ inB[5];
assign outB[7] = inA[7] ^ inB[3] ^ inB[5] ^ inB[7];

endmodule

module gf256_bf_0x15 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[0] ^ inB[3] ^ inB[6] ^ inB[7];
assign outB[0] = inA[0] ^ inB[3] ^ inB[6] ^ inB[7];
assign outA[1] = inA[1] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[6];
assign outB[1] = inA[1] ^ inB[2] ^ inB[3] ^ inB[6];
assign outA[2] = inA[2] ^ inB[0] ^ inB[5] ^ inB[7];
assign outB[2] = inA[2] ^ inB[0] ^ inB[2] ^ inB[5] ^ inB[7];
assign outA[3] = inA[3] ^ inB[1] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[3] = inA[3] ^ inB[1] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[4] = inA[4] ^ inB[0] ^ inB[7];
assign outB[4] = inA[4] ^ inB[0] ^ inB[4] ^ inB[7];
assign outA[5] = inA[5] ^ inB[1] ^ inB[6] ^ inB[7];
assign outB[5] = inA[5] ^ inB[1] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[6] = inA[6] ^ inB[2] ^ inB[4] ^ inB[6];
assign outB[6] = inA[6] ^ inB[2] ^ inB[4];
assign outA[7] = inA[7] ^ inB[3] ^ inB[5] ^ inB[7];
assign outB[7] = inA[7] ^ inB[3] ^ inB[5];

endmodule

module gf256_bf_0x16 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[1] ^ inB[3] ^ inB[6] ^ inB[7];
assign outB[0] = inA[0] ^ inB[0] ^ inB[1] ^ inB[3] ^ inB[6] ^ inB[7];
assign outA[1] = inA[1] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[6];
assign outB[1] = inA[1] ^ inB[0] ^ inB[2] ^ inB[3] ^ inB[6];
assign outA[2] = inA[2] ^ inB[0] ^ inB[2] ^ inB[3] ^ inB[5] ^ inB[7];
assign outB[2] = inA[2] ^ inB[0] ^ inB[3] ^ inB[5] ^ inB[7];
assign outA[3] = inA[3] ^ inB[1] ^ inB[2] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[3] = inA[3] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[4] = inA[4] ^ inB[0] ^ inB[4] ^ inB[5] ^ inB[7];
assign outB[4] = inA[4] ^ inB[0] ^ inB[5] ^ inB[7];
assign outA[5] = inA[5] ^ inB[1] ^ inB[4] ^ inB[6] ^ inB[7];
assign outB[5] = inA[5] ^ inB[1] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[6] = inA[6] ^ inB[2] ^ inB[4] ^ inB[7];
assign outB[6] = inA[6] ^ inB[2] ^ inB[4] ^ inB[6] ^ inB[7];
assign outA[7] = inA[7] ^ inB[3] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[7] = inA[7] ^ inB[3] ^ inB[5] ^ inB[6];

endmodule

module gf256_bf_0x17 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[0] ^ inB[1] ^ inB[3] ^ inB[6] ^ inB[7];
assign outB[0] = inA[0] ^ inB[1] ^ inB[3] ^ inB[6] ^ inB[7];
assign outA[1] = inA[1] ^ inB[0] ^ inB[2] ^ inB[3] ^ inB[6];
assign outB[1] = inA[1] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[6];
assign outA[2] = inA[2] ^ inB[0] ^ inB[3] ^ inB[5] ^ inB[7];
assign outB[2] = inA[2] ^ inB[0] ^ inB[2] ^ inB[3] ^ inB[5] ^ inB[7];
assign outA[3] = inA[3] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[3] = inA[3] ^ inB[1] ^ inB[2] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[4] = inA[4] ^ inB[0] ^ inB[5] ^ inB[7];
assign outB[4] = inA[4] ^ inB[0] ^ inB[4] ^ inB[5] ^ inB[7];
assign outA[5] = inA[5] ^ inB[1] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[5] = inA[5] ^ inB[1] ^ inB[4] ^ inB[6] ^ inB[7];
assign outA[6] = inA[6] ^ inB[2] ^ inB[4] ^ inB[6] ^ inB[7];
assign outB[6] = inA[6] ^ inB[2] ^ inB[4] ^ inB[7];
assign outA[7] = inA[7] ^ inB[3] ^ inB[5] ^ inB[6];
assign outB[7] = inA[7] ^ inB[3] ^ inB[5] ^ inB[6] ^ inB[7];

endmodule

module gf256_bf_0x18 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[2] ^ inB[3] ^ inB[6] ^ inB[7];
assign outB[0] = inA[0] ^ inB[0] ^ inB[2] ^ inB[3] ^ inB[6] ^ inB[7];
assign outA[1] = inA[1] ^ inB[2] ^ inB[6];
assign outB[1] = inA[1] ^ inB[1] ^ inB[2] ^ inB[6];
assign outA[2] = inA[2] ^ inB[1] ^ inB[3] ^ inB[5] ^ inB[7];
assign outB[2] = inA[2] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[5] ^ inB[7];
assign outA[3] = inA[3] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[3] = inA[3] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[4] = inA[4] ^ inB[0] ^ inB[4] ^ inB[6] ^ inB[7];
assign outB[4] = inA[4] ^ inB[0] ^ inB[6] ^ inB[7];
assign outA[5] = inA[5] ^ inB[1] ^ inB[5] ^ inB[6];
assign outB[5] = inA[5] ^ inB[1] ^ inB[6];
assign outA[6] = inA[6] ^ inB[2] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[6] = inA[6] ^ inB[2] ^ inB[5] ^ inB[7];
assign outA[7] = inA[7] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6];
assign outB[7] = inA[7] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];

endmodule

module gf256_bf_0x19 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[0] ^ inB[2] ^ inB[3] ^ inB[6] ^ inB[7];
assign outB[0] = inA[0] ^ inB[2] ^ inB[3] ^ inB[6] ^ inB[7];
assign outA[1] = inA[1] ^ inB[1] ^ inB[2] ^ inB[6];
assign outB[1] = inA[1] ^ inB[2] ^ inB[6];
assign outA[2] = inA[2] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[5] ^ inB[7];
assign outB[2] = inA[2] ^ inB[1] ^ inB[3] ^ inB[5] ^ inB[7];
assign outA[3] = inA[3] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[3] = inA[3] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[4] = inA[4] ^ inB[0] ^ inB[6] ^ inB[7];
assign outB[4] = inA[4] ^ inB[0] ^ inB[4] ^ inB[6] ^ inB[7];
assign outA[5] = inA[5] ^ inB[1] ^ inB[6];
assign outB[5] = inA[5] ^ inB[1] ^ inB[5] ^ inB[6];
assign outA[6] = inA[6] ^ inB[2] ^ inB[5] ^ inB[7];
assign outB[6] = inA[6] ^ inB[2] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[7] = inA[7] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[7] = inA[7] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6];

endmodule

module gf256_bf_0x1a (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[6] ^ inB[7];
assign outB[0] = inA[0] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[6] ^ inB[7];
assign outA[1] = inA[1] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[6];
assign outB[1] = inA[1] ^ inB[0] ^ inB[2] ^ inB[6];
assign outA[2] = inA[2] ^ inB[1] ^ inB[5] ^ inB[7];
assign outB[2] = inA[2] ^ inB[1] ^ inB[2] ^ inB[5] ^ inB[7];
assign outA[3] = inA[3] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[3] = inA[3] ^ inB[0] ^ inB[1] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[4] = inA[4] ^ inB[0] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[4] = inA[4] ^ inB[0] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[5] = inA[5] ^ inB[1] ^ inB[4] ^ inB[6];
assign outB[5] = inA[5] ^ inB[1] ^ inB[4] ^ inB[5] ^ inB[6];
assign outA[6] = inA[6] ^ inB[2] ^ inB[5] ^ inB[6];
assign outB[6] = inA[6] ^ inB[2] ^ inB[5];
assign outA[7] = inA[7] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[7];
assign outB[7] = inA[7] ^ inB[3] ^ inB[4] ^ inB[5];

endmodule

module gf256_bf_0x1b (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[6] ^ inB[7];
assign outB[0] = inA[0] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[6] ^ inB[7];
assign outA[1] = inA[1] ^ inB[0] ^ inB[2] ^ inB[6];
assign outB[1] = inA[1] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[6];
assign outA[2] = inA[2] ^ inB[1] ^ inB[2] ^ inB[5] ^ inB[7];
assign outB[2] = inA[2] ^ inB[1] ^ inB[5] ^ inB[7];
assign outA[3] = inA[3] ^ inB[0] ^ inB[1] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[3] = inA[3] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[4] = inA[4] ^ inB[0] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[4] = inA[4] ^ inB[0] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[5] = inA[5] ^ inB[1] ^ inB[4] ^ inB[5] ^ inB[6];
assign outB[5] = inA[5] ^ inB[1] ^ inB[4] ^ inB[6];
assign outA[6] = inA[6] ^ inB[2] ^ inB[5];
assign outB[6] = inA[6] ^ inB[2] ^ inB[5] ^ inB[6];
assign outA[7] = inA[7] ^ inB[3] ^ inB[4] ^ inB[5];
assign outB[7] = inA[7] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[7];

endmodule

module gf256_bf_0x1c (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[2] ^ inB[6] ^ inB[7];
assign outB[0] = inA[0] ^ inB[0] ^ inB[2] ^ inB[6] ^ inB[7];
assign outA[1] = inA[1] ^ inB[3] ^ inB[6];
assign outB[1] = inA[1] ^ inB[1] ^ inB[3] ^ inB[6];
assign outA[2] = inA[2] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[5] ^ inB[7];
assign outB[2] = inA[2] ^ inB[0] ^ inB[1] ^ inB[3] ^ inB[5] ^ inB[7];
assign outA[3] = inA[3] ^ inB[0] ^ inB[2] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[3] = inA[3] ^ inB[0] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[4] = inA[4] ^ inB[0] ^ inB[4] ^ inB[6];
assign outB[4] = inA[4] ^ inB[0] ^ inB[6];
assign outA[5] = inA[5] ^ inB[1] ^ inB[5] ^ inB[7];
assign outB[5] = inA[5] ^ inB[1] ^ inB[7];
assign outA[6] = inA[6] ^ inB[2] ^ inB[4] ^ inB[5] ^ inB[7];
assign outB[6] = inA[6] ^ inB[2] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[7] = inA[7] ^ inB[3] ^ inB[4] ^ inB[6] ^ inB[7];
assign outB[7] = inA[7] ^ inB[3] ^ inB[4] ^ inB[6];

endmodule

module gf256_bf_0x1d (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[0] ^ inB[2] ^ inB[6] ^ inB[7];
assign outB[0] = inA[0] ^ inB[2] ^ inB[6] ^ inB[7];
assign outA[1] = inA[1] ^ inB[1] ^ inB[3] ^ inB[6];
assign outB[1] = inA[1] ^ inB[3] ^ inB[6];
assign outA[2] = inA[2] ^ inB[0] ^ inB[1] ^ inB[3] ^ inB[5] ^ inB[7];
assign outB[2] = inA[2] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[5] ^ inB[7];
assign outA[3] = inA[3] ^ inB[0] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[3] = inA[3] ^ inB[0] ^ inB[2] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[4] = inA[4] ^ inB[0] ^ inB[6];
assign outB[4] = inA[4] ^ inB[0] ^ inB[4] ^ inB[6];
assign outA[5] = inA[5] ^ inB[1] ^ inB[7];
assign outB[5] = inA[5] ^ inB[1] ^ inB[5] ^ inB[7];
assign outA[6] = inA[6] ^ inB[2] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[6] = inA[6] ^ inB[2] ^ inB[4] ^ inB[5] ^ inB[7];
assign outA[7] = inA[7] ^ inB[3] ^ inB[4] ^ inB[6];
assign outB[7] = inA[7] ^ inB[3] ^ inB[4] ^ inB[6] ^ inB[7];

endmodule

module gf256_bf_0x1e (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[1] ^ inB[2] ^ inB[6] ^ inB[7];
assign outB[0] = inA[0] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[6] ^ inB[7];
assign outA[1] = inA[1] ^ inB[0] ^ inB[1] ^ inB[3] ^ inB[6];
assign outB[1] = inA[1] ^ inB[0] ^ inB[3] ^ inB[6];
assign outA[2] = inA[2] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[5] ^ inB[7];
assign outB[2] = inA[2] ^ inB[0] ^ inB[1] ^ inB[5] ^ inB[7];
assign outA[3] = inA[3] ^ inB[0] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[3] = inA[3] ^ inB[0] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[4] = inA[4] ^ inB[0] ^ inB[4] ^ inB[5] ^ inB[6];
assign outB[4] = inA[4] ^ inB[0] ^ inB[5] ^ inB[6];
assign outA[5] = inA[5] ^ inB[1] ^ inB[4] ^ inB[7];
assign outB[5] = inA[5] ^ inB[1] ^ inB[4] ^ inB[5] ^ inB[7];
assign outA[6] = inA[6] ^ inB[2] ^ inB[4] ^ inB[5];
assign outB[6] = inA[6] ^ inB[2] ^ inB[4] ^ inB[5] ^ inB[6];
assign outA[7] = inA[7] ^ inB[3] ^ inB[4];
assign outB[7] = inA[7] ^ inB[3] ^ inB[4] ^ inB[7];

endmodule

module gf256_bf_0x1f (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[6] ^ inB[7];
assign outB[0] = inA[0] ^ inB[1] ^ inB[2] ^ inB[6] ^ inB[7];
assign outA[1] = inA[1] ^ inB[0] ^ inB[3] ^ inB[6];
assign outB[1] = inA[1] ^ inB[0] ^ inB[1] ^ inB[3] ^ inB[6];
assign outA[2] = inA[2] ^ inB[0] ^ inB[1] ^ inB[5] ^ inB[7];
assign outB[2] = inA[2] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[5] ^ inB[7];
assign outA[3] = inA[3] ^ inB[0] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[3] = inA[3] ^ inB[0] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[4] = inA[4] ^ inB[0] ^ inB[5] ^ inB[6];
assign outB[4] = inA[4] ^ inB[0] ^ inB[4] ^ inB[5] ^ inB[6];
assign outA[5] = inA[5] ^ inB[1] ^ inB[4] ^ inB[5] ^ inB[7];
assign outB[5] = inA[5] ^ inB[1] ^ inB[4] ^ inB[7];
assign outA[6] = inA[6] ^ inB[2] ^ inB[4] ^ inB[5] ^ inB[6];
assign outB[6] = inA[6] ^ inB[2] ^ inB[4] ^ inB[5];
assign outA[7] = inA[7] ^ inB[3] ^ inB[4] ^ inB[7];
assign outB[7] = inA[7] ^ inB[3] ^ inB[4];

endmodule

module gf256_bf_0x20 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[6];
assign outB[0] = inA[0] ^ inB[0] ^ inB[6];
assign outA[1] = inA[1] ^ inB[7];
assign outB[1] = inA[1] ^ inB[1] ^ inB[7];
assign outA[2] = inA[2] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[2] = inA[2] ^ inB[2] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[3] = inA[3] ^ inB[4] ^ inB[6];
assign outB[3] = inA[3] ^ inB[3] ^ inB[4] ^ inB[6];
assign outA[4] = inA[4] ^ inB[1] ^ inB[5];
assign outB[4] = inA[4] ^ inB[1] ^ inB[4] ^ inB[5];
assign outA[5] = inA[5] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[5];
assign outB[5] = inA[5] ^ inB[0] ^ inB[1] ^ inB[4];
assign outA[6] = inA[6] ^ inB[3] ^ inB[7];
assign outB[6] = inA[6] ^ inB[3] ^ inB[6] ^ inB[7];
assign outA[7] = inA[7] ^ inB[2] ^ inB[3] ^ inB[6] ^ inB[7];
assign outB[7] = inA[7] ^ inB[2] ^ inB[3] ^ inB[6];

endmodule

module gf256_bf_0x21 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[0] ^ inB[6];
assign outB[0] = inA[0] ^ inB[6];
assign outA[1] = inA[1] ^ inB[1] ^ inB[7];
assign outB[1] = inA[1] ^ inB[7];
assign outA[2] = inA[2] ^ inB[2] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[2] = inA[2] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[3] = inA[3] ^ inB[3] ^ inB[4] ^ inB[6];
assign outB[3] = inA[3] ^ inB[4] ^ inB[6];
assign outA[4] = inA[4] ^ inB[1] ^ inB[4] ^ inB[5];
assign outB[4] = inA[4] ^ inB[1] ^ inB[5];
assign outA[5] = inA[5] ^ inB[0] ^ inB[1] ^ inB[4];
assign outB[5] = inA[5] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[5];
assign outA[6] = inA[6] ^ inB[3] ^ inB[6] ^ inB[7];
assign outB[6] = inA[6] ^ inB[3] ^ inB[7];
assign outA[7] = inA[7] ^ inB[2] ^ inB[3] ^ inB[6];
assign outB[7] = inA[7] ^ inB[2] ^ inB[3] ^ inB[6] ^ inB[7];

endmodule

module gf256_bf_0x22 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[1] ^ inB[6];
assign outB[0] = inA[0] ^ inB[0] ^ inB[1] ^ inB[6];
assign outA[1] = inA[1] ^ inB[0] ^ inB[1] ^ inB[7];
assign outB[1] = inA[1] ^ inB[0] ^ inB[7];
assign outA[2] = inA[2] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[2] = inA[2] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[3] = inA[3] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[6];
assign outB[3] = inA[3] ^ inB[2] ^ inB[4] ^ inB[6];
assign outA[4] = inA[4] ^ inB[1];
assign outB[4] = inA[4] ^ inB[1] ^ inB[4];
assign outA[5] = inA[5] ^ inB[0] ^ inB[1];
assign outB[5] = inA[5] ^ inB[0] ^ inB[1] ^ inB[5];
assign outA[6] = inA[6] ^ inB[3];
assign outB[6] = inA[6] ^ inB[3] ^ inB[6];
assign outA[7] = inA[7] ^ inB[2] ^ inB[3];
assign outB[7] = inA[7] ^ inB[2] ^ inB[3] ^ inB[7];

endmodule

module gf256_bf_0x23 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[0] ^ inB[1] ^ inB[6];
assign outB[0] = inA[0] ^ inB[1] ^ inB[6];
assign outA[1] = inA[1] ^ inB[0] ^ inB[7];
assign outB[1] = inA[1] ^ inB[0] ^ inB[1] ^ inB[7];
assign outA[2] = inA[2] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[2] = inA[2] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[3] = inA[3] ^ inB[2] ^ inB[4] ^ inB[6];
assign outB[3] = inA[3] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[6];
assign outA[4] = inA[4] ^ inB[1] ^ inB[4];
assign outB[4] = inA[4] ^ inB[1];
assign outA[5] = inA[5] ^ inB[0] ^ inB[1] ^ inB[5];
assign outB[5] = inA[5] ^ inB[0] ^ inB[1];
assign outA[6] = inA[6] ^ inB[3] ^ inB[6];
assign outB[6] = inA[6] ^ inB[3];
assign outA[7] = inA[7] ^ inB[2] ^ inB[3] ^ inB[7];
assign outB[7] = inA[7] ^ inB[2] ^ inB[3];

endmodule

module gf256_bf_0x24 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[3] ^ inB[6];
assign outB[0] = inA[0] ^ inB[0] ^ inB[3] ^ inB[6];
assign outA[1] = inA[1] ^ inB[2] ^ inB[3] ^ inB[7];
assign outB[1] = inA[1] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[7];
assign outA[2] = inA[2] ^ inB[0] ^ inB[2] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[2] = inA[2] ^ inB[0] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[3] = inA[3] ^ inB[1] ^ inB[3] ^ inB[4] ^ inB[6];
assign outB[3] = inA[3] ^ inB[1] ^ inB[4] ^ inB[6];
assign outA[4] = inA[4] ^ inB[1] ^ inB[5] ^ inB[7];
assign outB[4] = inA[4] ^ inB[1] ^ inB[4] ^ inB[5] ^ inB[7];
assign outA[5] = inA[5] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[5] = inA[5] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[6] ^ inB[7];
assign outA[6] = inA[6] ^ inB[3] ^ inB[4] ^ inB[6] ^ inB[7];
assign outB[6] = inA[6] ^ inB[3] ^ inB[4] ^ inB[7];
assign outA[7] = inA[7] ^ inB[2] ^ inB[3] ^ inB[5] ^ inB[6];
assign outB[7] = inA[7] ^ inB[2] ^ inB[3] ^ inB[5] ^ inB[6] ^ inB[7];

endmodule

module gf256_bf_0x25 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[0] ^ inB[3] ^ inB[6];
assign outB[0] = inA[0] ^ inB[3] ^ inB[6];
assign outA[1] = inA[1] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[7];
assign outB[1] = inA[1] ^ inB[2] ^ inB[3] ^ inB[7];
assign outA[2] = inA[2] ^ inB[0] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[2] = inA[2] ^ inB[0] ^ inB[2] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[3] = inA[3] ^ inB[1] ^ inB[4] ^ inB[6];
assign outB[3] = inA[3] ^ inB[1] ^ inB[3] ^ inB[4] ^ inB[6];
assign outA[4] = inA[4] ^ inB[1] ^ inB[4] ^ inB[5] ^ inB[7];
assign outB[4] = inA[4] ^ inB[1] ^ inB[5] ^ inB[7];
assign outA[5] = inA[5] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[6] ^ inB[7];
assign outB[5] = inA[5] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[6] = inA[6] ^ inB[3] ^ inB[4] ^ inB[7];
assign outB[6] = inA[6] ^ inB[3] ^ inB[4] ^ inB[6] ^ inB[7];
assign outA[7] = inA[7] ^ inB[2] ^ inB[3] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[7] = inA[7] ^ inB[2] ^ inB[3] ^ inB[5] ^ inB[6];

endmodule

module gf256_bf_0x26 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[1] ^ inB[3] ^ inB[6];
assign outB[0] = inA[0] ^ inB[0] ^ inB[1] ^ inB[3] ^ inB[6];
assign outA[1] = inA[1] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[7];
assign outB[1] = inA[1] ^ inB[0] ^ inB[2] ^ inB[3] ^ inB[7];
assign outA[2] = inA[2] ^ inB[0] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[2] = inA[2] ^ inB[0] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[3] = inA[3] ^ inB[1] ^ inB[2] ^ inB[4] ^ inB[6];
assign outB[3] = inA[3] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[6];
assign outA[4] = inA[4] ^ inB[1] ^ inB[7];
assign outB[4] = inA[4] ^ inB[1] ^ inB[4] ^ inB[7];
assign outA[5] = inA[5] ^ inB[0] ^ inB[1] ^ inB[6] ^ inB[7];
assign outB[5] = inA[5] ^ inB[0] ^ inB[1] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[6] = inA[6] ^ inB[3] ^ inB[4] ^ inB[6];
assign outB[6] = inA[6] ^ inB[3] ^ inB[4];
assign outA[7] = inA[7] ^ inB[2] ^ inB[3] ^ inB[5] ^ inB[7];
assign outB[7] = inA[7] ^ inB[2] ^ inB[3] ^ inB[5];

endmodule

module gf256_bf_0x27 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[0] ^ inB[1] ^ inB[3] ^ inB[6];
assign outB[0] = inA[0] ^ inB[1] ^ inB[3] ^ inB[6];
assign outA[1] = inA[1] ^ inB[0] ^ inB[2] ^ inB[3] ^ inB[7];
assign outB[1] = inA[1] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[7];
assign outA[2] = inA[2] ^ inB[0] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[2] = inA[2] ^ inB[0] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[3] = inA[3] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[6];
assign outB[3] = inA[3] ^ inB[1] ^ inB[2] ^ inB[4] ^ inB[6];
assign outA[4] = inA[4] ^ inB[1] ^ inB[4] ^ inB[7];
assign outB[4] = inA[4] ^ inB[1] ^ inB[7];
assign outA[5] = inA[5] ^ inB[0] ^ inB[1] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[5] = inA[5] ^ inB[0] ^ inB[1] ^ inB[6] ^ inB[7];
assign outA[6] = inA[6] ^ inB[3] ^ inB[4];
assign outB[6] = inA[6] ^ inB[3] ^ inB[4] ^ inB[6];
assign outA[7] = inA[7] ^ inB[2] ^ inB[3] ^ inB[5];
assign outB[7] = inA[7] ^ inB[2] ^ inB[3] ^ inB[5] ^ inB[7];

endmodule

module gf256_bf_0x28 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[2] ^ inB[3] ^ inB[6];
assign outB[0] = inA[0] ^ inB[0] ^ inB[2] ^ inB[3] ^ inB[6];
assign outA[1] = inA[1] ^ inB[2] ^ inB[7];
assign outB[1] = inA[1] ^ inB[1] ^ inB[2] ^ inB[7];
assign outA[2] = inA[2] ^ inB[1] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[2] = inA[2] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[3] = inA[3] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[6];
assign outB[3] = inA[3] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[4] ^ inB[6];
assign outA[4] = inA[4] ^ inB[1] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[4] = inA[4] ^ inB[1] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[5] = inA[5] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[5] ^ inB[6];
assign outB[5] = inA[5] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[6];
assign outA[6] = inA[6] ^ inB[3] ^ inB[5];
assign outB[6] = inA[6] ^ inB[3] ^ inB[5] ^ inB[6];
assign outA[7] = inA[7] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[5];
assign outB[7] = inA[7] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[7];

endmodule

module gf256_bf_0x29 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[0] ^ inB[2] ^ inB[3] ^ inB[6];
assign outB[0] = inA[0] ^ inB[2] ^ inB[3] ^ inB[6];
assign outA[1] = inA[1] ^ inB[1] ^ inB[2] ^ inB[7];
assign outB[1] = inA[1] ^ inB[2] ^ inB[7];
assign outA[2] = inA[2] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[2] = inA[2] ^ inB[1] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[3] = inA[3] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[4] ^ inB[6];
assign outB[3] = inA[3] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[6];
assign outA[4] = inA[4] ^ inB[1] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[4] = inA[4] ^ inB[1] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[5] = inA[5] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[6];
assign outB[5] = inA[5] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[5] ^ inB[6];
assign outA[6] = inA[6] ^ inB[3] ^ inB[5] ^ inB[6];
assign outB[6] = inA[6] ^ inB[3] ^ inB[5];
assign outA[7] = inA[7] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[7];
assign outB[7] = inA[7] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[5];

endmodule

module gf256_bf_0x2a (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[6];
assign outB[0] = inA[0] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[6];
assign outA[1] = inA[1] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[7];
assign outB[1] = inA[1] ^ inB[0] ^ inB[2] ^ inB[7];
assign outA[2] = inA[2] ^ inB[1] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[2] = inA[2] ^ inB[1] ^ inB[2] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[3] = inA[3] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[6];
assign outB[3] = inA[3] ^ inB[0] ^ inB[1] ^ inB[3] ^ inB[4] ^ inB[6];
assign outA[4] = inA[4] ^ inB[1] ^ inB[6] ^ inB[7];
assign outB[4] = inA[4] ^ inB[1] ^ inB[4] ^ inB[6] ^ inB[7];
assign outA[5] = inA[5] ^ inB[0] ^ inB[1] ^ inB[6];
assign outB[5] = inA[5] ^ inB[0] ^ inB[1] ^ inB[5] ^ inB[6];
assign outA[6] = inA[6] ^ inB[3] ^ inB[5] ^ inB[7];
assign outB[6] = inA[6] ^ inB[3] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[7] = inA[7] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[7] = inA[7] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6];

endmodule

module gf256_bf_0x2b (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[6];
assign outB[0] = inA[0] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[6];
assign outA[1] = inA[1] ^ inB[0] ^ inB[2] ^ inB[7];
assign outB[1] = inA[1] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[7];
assign outA[2] = inA[2] ^ inB[1] ^ inB[2] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[2] = inA[2] ^ inB[1] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[3] = inA[3] ^ inB[0] ^ inB[1] ^ inB[3] ^ inB[4] ^ inB[6];
assign outB[3] = inA[3] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[6];
assign outA[4] = inA[4] ^ inB[1] ^ inB[4] ^ inB[6] ^ inB[7];
assign outB[4] = inA[4] ^ inB[1] ^ inB[6] ^ inB[7];
assign outA[5] = inA[5] ^ inB[0] ^ inB[1] ^ inB[5] ^ inB[6];
assign outB[5] = inA[5] ^ inB[0] ^ inB[1] ^ inB[6];
assign outA[6] = inA[6] ^ inB[3] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[6] = inA[6] ^ inB[3] ^ inB[5] ^ inB[7];
assign outA[7] = inA[7] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6];
assign outB[7] = inA[7] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];

endmodule

module gf256_bf_0x2c (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[2] ^ inB[6];
assign outB[0] = inA[0] ^ inB[0] ^ inB[2] ^ inB[6];
assign outA[1] = inA[1] ^ inB[3] ^ inB[7];
assign outB[1] = inA[1] ^ inB[1] ^ inB[3] ^ inB[7];
assign outA[2] = inA[2] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[2] = inA[2] ^ inB[0] ^ inB[1] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[3] = inA[3] ^ inB[0] ^ inB[2] ^ inB[4] ^ inB[6];
assign outB[3] = inA[3] ^ inB[0] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[6];
assign outA[4] = inA[4] ^ inB[1] ^ inB[5] ^ inB[6];
assign outB[4] = inA[4] ^ inB[1] ^ inB[4] ^ inB[5] ^ inB[6];
assign outA[5] = inA[5] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[5] ^ inB[7];
assign outB[5] = inA[5] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[7];
assign outA[6] = inA[6] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6];
assign outB[6] = inA[6] ^ inB[3] ^ inB[4] ^ inB[5];
assign outA[7] = inA[7] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[7];
assign outB[7] = inA[7] ^ inB[2] ^ inB[3] ^ inB[4];

endmodule

module gf256_bf_0x2d (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[0] ^ inB[2] ^ inB[6];
assign outB[0] = inA[0] ^ inB[2] ^ inB[6];
assign outA[1] = inA[1] ^ inB[1] ^ inB[3] ^ inB[7];
assign outB[1] = inA[1] ^ inB[3] ^ inB[7];
assign outA[2] = inA[2] ^ inB[0] ^ inB[1] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[2] = inA[2] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[3] = inA[3] ^ inB[0] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[6];
assign outB[3] = inA[3] ^ inB[0] ^ inB[2] ^ inB[4] ^ inB[6];
assign outA[4] = inA[4] ^ inB[1] ^ inB[4] ^ inB[5] ^ inB[6];
assign outB[4] = inA[4] ^ inB[1] ^ inB[5] ^ inB[6];
assign outA[5] = inA[5] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[7];
assign outB[5] = inA[5] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[5] ^ inB[7];
assign outA[6] = inA[6] ^ inB[3] ^ inB[4] ^ inB[5];
assign outB[6] = inA[6] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6];
assign outA[7] = inA[7] ^ inB[2] ^ inB[3] ^ inB[4];
assign outB[7] = inA[7] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[7];

endmodule

module gf256_bf_0x2e (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[1] ^ inB[2] ^ inB[6];
assign outB[0] = inA[0] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[6];
assign outA[1] = inA[1] ^ inB[0] ^ inB[1] ^ inB[3] ^ inB[7];
assign outB[1] = inA[1] ^ inB[0] ^ inB[3] ^ inB[7];
assign outA[2] = inA[2] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[2] = inA[2] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[3] = inA[3] ^ inB[0] ^ inB[3] ^ inB[4] ^ inB[6];
assign outB[3] = inA[3] ^ inB[0] ^ inB[4] ^ inB[6];
assign outA[4] = inA[4] ^ inB[1] ^ inB[6];
assign outB[4] = inA[4] ^ inB[1] ^ inB[4] ^ inB[6];
assign outA[5] = inA[5] ^ inB[0] ^ inB[1] ^ inB[7];
assign outB[5] = inA[5] ^ inB[0] ^ inB[1] ^ inB[5] ^ inB[7];
assign outA[6] = inA[6] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[6] = inA[6] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[7];
assign outA[7] = inA[7] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[6];
assign outB[7] = inA[7] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[6] ^ inB[7];

endmodule

module gf256_bf_0x2f (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[6];
assign outB[0] = inA[0] ^ inB[1] ^ inB[2] ^ inB[6];
assign outA[1] = inA[1] ^ inB[0] ^ inB[3] ^ inB[7];
assign outB[1] = inA[1] ^ inB[0] ^ inB[1] ^ inB[3] ^ inB[7];
assign outA[2] = inA[2] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[2] = inA[2] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[3] = inA[3] ^ inB[0] ^ inB[4] ^ inB[6];
assign outB[3] = inA[3] ^ inB[0] ^ inB[3] ^ inB[4] ^ inB[6];
assign outA[4] = inA[4] ^ inB[1] ^ inB[4] ^ inB[6];
assign outB[4] = inA[4] ^ inB[1] ^ inB[6];
assign outA[5] = inA[5] ^ inB[0] ^ inB[1] ^ inB[5] ^ inB[7];
assign outB[5] = inA[5] ^ inB[0] ^ inB[1] ^ inB[7];
assign outA[6] = inA[6] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[7];
assign outB[6] = inA[6] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[7] = inA[7] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[6] ^ inB[7];
assign outB[7] = inA[7] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[6];

endmodule

module gf256_bf_0x30 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[7];
assign outB[0] = inA[0] ^ inB[0] ^ inB[7];
assign outA[1] = inA[1] ^ inB[6] ^ inB[7];
assign outB[1] = inA[1] ^ inB[1] ^ inB[6] ^ inB[7];
assign outA[2] = inA[2] ^ inB[4] ^ inB[6];
assign outB[2] = inA[2] ^ inB[2] ^ inB[4] ^ inB[6];
assign outA[3] = inA[3] ^ inB[5] ^ inB[7];
assign outB[3] = inA[3] ^ inB[3] ^ inB[5] ^ inB[7];
assign outA[4] = inA[4] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[5];
assign outB[4] = inA[4] ^ inB[0] ^ inB[1] ^ inB[5];
assign outA[5] = inA[5] ^ inB[0] ^ inB[4];
assign outB[5] = inA[5] ^ inB[0] ^ inB[4] ^ inB[5];
assign outA[6] = inA[6] ^ inB[2] ^ inB[3] ^ inB[6] ^ inB[7];
assign outB[6] = inA[6] ^ inB[2] ^ inB[3] ^ inB[7];
assign outA[7] = inA[7] ^ inB[2] ^ inB[6];
assign outB[7] = inA[7] ^ inB[2] ^ inB[6] ^ inB[7];

endmodule

module gf256_bf_0x31 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[0] ^ inB[7];
assign outB[0] = inA[0] ^ inB[7];
assign outA[1] = inA[1] ^ inB[1] ^ inB[6] ^ inB[7];
assign outB[1] = inA[1] ^ inB[6] ^ inB[7];
assign outA[2] = inA[2] ^ inB[2] ^ inB[4] ^ inB[6];
assign outB[2] = inA[2] ^ inB[4] ^ inB[6];
assign outA[3] = inA[3] ^ inB[3] ^ inB[5] ^ inB[7];
assign outB[3] = inA[3] ^ inB[5] ^ inB[7];
assign outA[4] = inA[4] ^ inB[0] ^ inB[1] ^ inB[5];
assign outB[4] = inA[4] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[5];
assign outA[5] = inA[5] ^ inB[0] ^ inB[4] ^ inB[5];
assign outB[5] = inA[5] ^ inB[0] ^ inB[4];
assign outA[6] = inA[6] ^ inB[2] ^ inB[3] ^ inB[7];
assign outB[6] = inA[6] ^ inB[2] ^ inB[3] ^ inB[6] ^ inB[7];
assign outA[7] = inA[7] ^ inB[2] ^ inB[6] ^ inB[7];
assign outB[7] = inA[7] ^ inB[2] ^ inB[6];

endmodule

module gf256_bf_0x32 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[1] ^ inB[7];
assign outB[0] = inA[0] ^ inB[0] ^ inB[1] ^ inB[7];
assign outA[1] = inA[1] ^ inB[0] ^ inB[1] ^ inB[6] ^ inB[7];
assign outB[1] = inA[1] ^ inB[0] ^ inB[6] ^ inB[7];
assign outA[2] = inA[2] ^ inB[3] ^ inB[4] ^ inB[6];
assign outB[2] = inA[2] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[6];
assign outA[3] = inA[3] ^ inB[2] ^ inB[3] ^ inB[5] ^ inB[7];
assign outB[3] = inA[3] ^ inB[2] ^ inB[5] ^ inB[7];
assign outA[4] = inA[4] ^ inB[0] ^ inB[1] ^ inB[4];
assign outB[4] = inA[4] ^ inB[0] ^ inB[1];
assign outA[5] = inA[5] ^ inB[0] ^ inB[5];
assign outB[5] = inA[5] ^ inB[0];
assign outA[6] = inA[6] ^ inB[2] ^ inB[3] ^ inB[6];
assign outB[6] = inA[6] ^ inB[2] ^ inB[3];
assign outA[7] = inA[7] ^ inB[2] ^ inB[7];
assign outB[7] = inA[7] ^ inB[2];

endmodule

module gf256_bf_0x33 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[0] ^ inB[1] ^ inB[7];
assign outB[0] = inA[0] ^ inB[1] ^ inB[7];
assign outA[1] = inA[1] ^ inB[0] ^ inB[6] ^ inB[7];
assign outB[1] = inA[1] ^ inB[0] ^ inB[1] ^ inB[6] ^ inB[7];
assign outA[2] = inA[2] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[6];
assign outB[2] = inA[2] ^ inB[3] ^ inB[4] ^ inB[6];
assign outA[3] = inA[3] ^ inB[2] ^ inB[5] ^ inB[7];
assign outB[3] = inA[3] ^ inB[2] ^ inB[3] ^ inB[5] ^ inB[7];
assign outA[4] = inA[4] ^ inB[0] ^ inB[1];
assign outB[4] = inA[4] ^ inB[0] ^ inB[1] ^ inB[4];
assign outA[5] = inA[5] ^ inB[0];
assign outB[5] = inA[5] ^ inB[0] ^ inB[5];
assign outA[6] = inA[6] ^ inB[2] ^ inB[3];
assign outB[6] = inA[6] ^ inB[2] ^ inB[3] ^ inB[6];
assign outA[7] = inA[7] ^ inB[2];
assign outB[7] = inA[7] ^ inB[2] ^ inB[7];

endmodule

module gf256_bf_0x34 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[3] ^ inB[7];
assign outB[0] = inA[0] ^ inB[0] ^ inB[3] ^ inB[7];
assign outA[1] = inA[1] ^ inB[2] ^ inB[3] ^ inB[6] ^ inB[7];
assign outB[1] = inA[1] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[6] ^ inB[7];
assign outA[2] = inA[2] ^ inB[0] ^ inB[2] ^ inB[4] ^ inB[6];
assign outB[2] = inA[2] ^ inB[0] ^ inB[4] ^ inB[6];
assign outA[3] = inA[3] ^ inB[1] ^ inB[3] ^ inB[5] ^ inB[7];
assign outB[3] = inA[3] ^ inB[1] ^ inB[5] ^ inB[7];
assign outA[4] = inA[4] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[5] ^ inB[7];
assign outB[4] = inA[4] ^ inB[0] ^ inB[1] ^ inB[5] ^ inB[7];
assign outA[5] = inA[5] ^ inB[0] ^ inB[4] ^ inB[6] ^ inB[7];
assign outB[5] = inA[5] ^ inB[0] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[6] = inA[6] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[7];
assign outB[6] = inA[6] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[6] ^ inB[7];
assign outA[7] = inA[7] ^ inB[2] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[7] = inA[7] ^ inB[2] ^ inB[5] ^ inB[6];

endmodule

module gf256_bf_0x35 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[0] ^ inB[3] ^ inB[7];
assign outB[0] = inA[0] ^ inB[3] ^ inB[7];
assign outA[1] = inA[1] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[6] ^ inB[7];
assign outB[1] = inA[1] ^ inB[2] ^ inB[3] ^ inB[6] ^ inB[7];
assign outA[2] = inA[2] ^ inB[0] ^ inB[4] ^ inB[6];
assign outB[2] = inA[2] ^ inB[0] ^ inB[2] ^ inB[4] ^ inB[6];
assign outA[3] = inA[3] ^ inB[1] ^ inB[5] ^ inB[7];
assign outB[3] = inA[3] ^ inB[1] ^ inB[3] ^ inB[5] ^ inB[7];
assign outA[4] = inA[4] ^ inB[0] ^ inB[1] ^ inB[5] ^ inB[7];
assign outB[4] = inA[4] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[5] ^ inB[7];
assign outA[5] = inA[5] ^ inB[0] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[5] = inA[5] ^ inB[0] ^ inB[4] ^ inB[6] ^ inB[7];
assign outA[6] = inA[6] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[6] ^ inB[7];
assign outB[6] = inA[6] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[7];
assign outA[7] = inA[7] ^ inB[2] ^ inB[5] ^ inB[6];
assign outB[7] = inA[7] ^ inB[2] ^ inB[5] ^ inB[6] ^ inB[7];

endmodule

module gf256_bf_0x36 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[1] ^ inB[3] ^ inB[7];
assign outB[0] = inA[0] ^ inB[0] ^ inB[1] ^ inB[3] ^ inB[7];
assign outA[1] = inA[1] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[6] ^ inB[7];
assign outB[1] = inA[1] ^ inB[0] ^ inB[2] ^ inB[3] ^ inB[6] ^ inB[7];
assign outA[2] = inA[2] ^ inB[0] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[6];
assign outB[2] = inA[2] ^ inB[0] ^ inB[3] ^ inB[4] ^ inB[6];
assign outA[3] = inA[3] ^ inB[1] ^ inB[2] ^ inB[5] ^ inB[7];
assign outB[3] = inA[3] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[5] ^ inB[7];
assign outA[4] = inA[4] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[7];
assign outB[4] = inA[4] ^ inB[0] ^ inB[1] ^ inB[7];
assign outA[5] = inA[5] ^ inB[0] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[5] = inA[5] ^ inB[0] ^ inB[6] ^ inB[7];
assign outA[6] = inA[6] ^ inB[2] ^ inB[3] ^ inB[4];
assign outB[6] = inA[6] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[6];
assign outA[7] = inA[7] ^ inB[2] ^ inB[5];
assign outB[7] = inA[7] ^ inB[2] ^ inB[5] ^ inB[7];

endmodule

module gf256_bf_0x37 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[0] ^ inB[1] ^ inB[3] ^ inB[7];
assign outB[0] = inA[0] ^ inB[1] ^ inB[3] ^ inB[7];
assign outA[1] = inA[1] ^ inB[0] ^ inB[2] ^ inB[3] ^ inB[6] ^ inB[7];
assign outB[1] = inA[1] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[6] ^ inB[7];
assign outA[2] = inA[2] ^ inB[0] ^ inB[3] ^ inB[4] ^ inB[6];
assign outB[2] = inA[2] ^ inB[0] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[6];
assign outA[3] = inA[3] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[5] ^ inB[7];
assign outB[3] = inA[3] ^ inB[1] ^ inB[2] ^ inB[5] ^ inB[7];
assign outA[4] = inA[4] ^ inB[0] ^ inB[1] ^ inB[7];
assign outB[4] = inA[4] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[7];
assign outA[5] = inA[5] ^ inB[0] ^ inB[6] ^ inB[7];
assign outB[5] = inA[5] ^ inB[0] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[6] = inA[6] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[6];
assign outB[6] = inA[6] ^ inB[2] ^ inB[3] ^ inB[4];
assign outA[7] = inA[7] ^ inB[2] ^ inB[5] ^ inB[7];
assign outB[7] = inA[7] ^ inB[2] ^ inB[5];

endmodule

module gf256_bf_0x38 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[2] ^ inB[3] ^ inB[7];
assign outB[0] = inA[0] ^ inB[0] ^ inB[2] ^ inB[3] ^ inB[7];
assign outA[1] = inA[1] ^ inB[2] ^ inB[6] ^ inB[7];
assign outB[1] = inA[1] ^ inB[1] ^ inB[2] ^ inB[6] ^ inB[7];
assign outA[2] = inA[2] ^ inB[1] ^ inB[3] ^ inB[4] ^ inB[6];
assign outB[2] = inA[2] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[6];
assign outA[3] = inA[3] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[5] ^ inB[7];
assign outB[3] = inA[3] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[5] ^ inB[7];
assign outA[4] = inA[4] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[4] = inA[4] ^ inB[0] ^ inB[1] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[5] = inA[5] ^ inB[0] ^ inB[4] ^ inB[6];
assign outB[5] = inA[5] ^ inB[0] ^ inB[4] ^ inB[5] ^ inB[6];
assign outA[6] = inA[6] ^ inB[2] ^ inB[3] ^ inB[5] ^ inB[6];
assign outB[6] = inA[6] ^ inB[2] ^ inB[3] ^ inB[5];
assign outA[7] = inA[7] ^ inB[2] ^ inB[4] ^ inB[5] ^ inB[7];
assign outB[7] = inA[7] ^ inB[2] ^ inB[4] ^ inB[5];

endmodule

module gf256_bf_0x39 (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[0] ^ inB[2] ^ inB[3] ^ inB[7];
assign outB[0] = inA[0] ^ inB[2] ^ inB[3] ^ inB[7];
assign outA[1] = inA[1] ^ inB[1] ^ inB[2] ^ inB[6] ^ inB[7];
assign outB[1] = inA[1] ^ inB[2] ^ inB[6] ^ inB[7];
assign outA[2] = inA[2] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[6];
assign outB[2] = inA[2] ^ inB[1] ^ inB[3] ^ inB[4] ^ inB[6];
assign outA[3] = inA[3] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[5] ^ inB[7];
assign outB[3] = inA[3] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[5] ^ inB[7];
assign outA[4] = inA[4] ^ inB[0] ^ inB[1] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[4] = inA[4] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[5] = inA[5] ^ inB[0] ^ inB[4] ^ inB[5] ^ inB[6];
assign outB[5] = inA[5] ^ inB[0] ^ inB[4] ^ inB[6];
assign outA[6] = inA[6] ^ inB[2] ^ inB[3] ^ inB[5];
assign outB[6] = inA[6] ^ inB[2] ^ inB[3] ^ inB[5] ^ inB[6];
assign outA[7] = inA[7] ^ inB[2] ^ inB[4] ^ inB[5];
assign outB[7] = inA[7] ^ inB[2] ^ inB[4] ^ inB[5] ^ inB[7];

endmodule

module gf256_bf_0x3a (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[7];
assign outB[0] = inA[0] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[7];
assign outA[1] = inA[1] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[6] ^ inB[7];
assign outB[1] = inA[1] ^ inB[0] ^ inB[2] ^ inB[6] ^ inB[7];
assign outA[2] = inA[2] ^ inB[1] ^ inB[4] ^ inB[6];
assign outB[2] = inA[2] ^ inB[1] ^ inB[2] ^ inB[4] ^ inB[6];
assign outA[3] = inA[3] ^ inB[0] ^ inB[1] ^ inB[5] ^ inB[7];
assign outB[3] = inA[3] ^ inB[0] ^ inB[1] ^ inB[3] ^ inB[5] ^ inB[7];
assign outA[4] = inA[4] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[6] ^ inB[7];
assign outB[4] = inA[4] ^ inB[0] ^ inB[1] ^ inB[6] ^ inB[7];
assign outA[5] = inA[5] ^ inB[0] ^ inB[5] ^ inB[6];
assign outB[5] = inA[5] ^ inB[0] ^ inB[6];
assign outA[6] = inA[6] ^ inB[2] ^ inB[3] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[6] = inA[6] ^ inB[2] ^ inB[3] ^ inB[5] ^ inB[7];
assign outA[7] = inA[7] ^ inB[2] ^ inB[4] ^ inB[5] ^ inB[6];
assign outB[7] = inA[7] ^ inB[2] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];

endmodule

module gf256_bf_0x3b (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[7];
assign outB[0] = inA[0] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[7];
assign outA[1] = inA[1] ^ inB[0] ^ inB[2] ^ inB[6] ^ inB[7];
assign outB[1] = inA[1] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[6] ^ inB[7];
assign outA[2] = inA[2] ^ inB[1] ^ inB[2] ^ inB[4] ^ inB[6];
assign outB[2] = inA[2] ^ inB[1] ^ inB[4] ^ inB[6];
assign outA[3] = inA[3] ^ inB[0] ^ inB[1] ^ inB[3] ^ inB[5] ^ inB[7];
assign outB[3] = inA[3] ^ inB[0] ^ inB[1] ^ inB[5] ^ inB[7];
assign outA[4] = inA[4] ^ inB[0] ^ inB[1] ^ inB[6] ^ inB[7];
assign outB[4] = inA[4] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[6] ^ inB[7];
assign outA[5] = inA[5] ^ inB[0] ^ inB[6];
assign outB[5] = inA[5] ^ inB[0] ^ inB[5] ^ inB[6];
assign outA[6] = inA[6] ^ inB[2] ^ inB[3] ^ inB[5] ^ inB[7];
assign outB[6] = inA[6] ^ inB[2] ^ inB[3] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[7] = inA[7] ^ inB[2] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[7] = inA[7] ^ inB[2] ^ inB[4] ^ inB[5] ^ inB[6];

endmodule

module gf256_bf_0x3c (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[2] ^ inB[7];
assign outB[0] = inA[0] ^ inB[0] ^ inB[2] ^ inB[7];
assign outA[1] = inA[1] ^ inB[3] ^ inB[6] ^ inB[7];
assign outB[1] = inA[1] ^ inB[1] ^ inB[3] ^ inB[6] ^ inB[7];
assign outA[2] = inA[2] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[6];
assign outB[2] = inA[2] ^ inB[0] ^ inB[1] ^ inB[3] ^ inB[4] ^ inB[6];
assign outA[3] = inA[3] ^ inB[0] ^ inB[2] ^ inB[5] ^ inB[7];
assign outB[3] = inA[3] ^ inB[0] ^ inB[2] ^ inB[3] ^ inB[5] ^ inB[7];
assign outA[4] = inA[4] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[5] ^ inB[6];
assign outB[4] = inA[4] ^ inB[0] ^ inB[1] ^ inB[5] ^ inB[6];
assign outA[5] = inA[5] ^ inB[0] ^ inB[4] ^ inB[7];
assign outB[5] = inA[5] ^ inB[0] ^ inB[4] ^ inB[5] ^ inB[7];
assign outA[6] = inA[6] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[5];
assign outB[6] = inA[6] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6];
assign outA[7] = inA[7] ^ inB[2] ^ inB[4];
assign outB[7] = inA[7] ^ inB[2] ^ inB[4] ^ inB[7];

endmodule

module gf256_bf_0x3d (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[0] ^ inB[2] ^ inB[7];
assign outB[0] = inA[0] ^ inB[2] ^ inB[7];
assign outA[1] = inA[1] ^ inB[1] ^ inB[3] ^ inB[6] ^ inB[7];
assign outB[1] = inA[1] ^ inB[3] ^ inB[6] ^ inB[7];
assign outA[2] = inA[2] ^ inB[0] ^ inB[1] ^ inB[3] ^ inB[4] ^ inB[6];
assign outB[2] = inA[2] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[6];
assign outA[3] = inA[3] ^ inB[0] ^ inB[2] ^ inB[3] ^ inB[5] ^ inB[7];
assign outB[3] = inA[3] ^ inB[0] ^ inB[2] ^ inB[5] ^ inB[7];
assign outA[4] = inA[4] ^ inB[0] ^ inB[1] ^ inB[5] ^ inB[6];
assign outB[4] = inA[4] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[5] ^ inB[6];
assign outA[5] = inA[5] ^ inB[0] ^ inB[4] ^ inB[5] ^ inB[7];
assign outB[5] = inA[5] ^ inB[0] ^ inB[4] ^ inB[7];
assign outA[6] = inA[6] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6];
assign outB[6] = inA[6] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[5];
assign outA[7] = inA[7] ^ inB[2] ^ inB[4] ^ inB[7];
assign outB[7] = inA[7] ^ inB[2] ^ inB[4];

endmodule

module gf256_bf_0x3e (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[1] ^ inB[2] ^ inB[7];
assign outB[0] = inA[0] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[7];
assign outA[1] = inA[1] ^ inB[0] ^ inB[1] ^ inB[3] ^ inB[6] ^ inB[7];
assign outB[1] = inA[1] ^ inB[0] ^ inB[3] ^ inB[6] ^ inB[7];
assign outA[2] = inA[2] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[4] ^ inB[6];
assign outB[2] = inA[2] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[6];
assign outA[3] = inA[3] ^ inB[0] ^ inB[3] ^ inB[5] ^ inB[7];
assign outB[3] = inA[3] ^ inB[0] ^ inB[5] ^ inB[7];
assign outA[4] = inA[4] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[6];
assign outB[4] = inA[4] ^ inB[0] ^ inB[1] ^ inB[6];
assign outA[5] = inA[5] ^ inB[0] ^ inB[5] ^ inB[7];
assign outB[5] = inA[5] ^ inB[0] ^ inB[7];
assign outA[6] = inA[6] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[7];
assign outB[6] = inA[6] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outA[7] = inA[7] ^ inB[2] ^ inB[4] ^ inB[6] ^ inB[7];
assign outB[7] = inA[7] ^ inB[2] ^ inB[4] ^ inB[6];

endmodule

module gf256_bf_0x3f (
	inA,
	inB,
	outA,
	outB
);
input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outA;
output		[7:0]		outB;

assign outA[0] = inA[0] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[7];
assign outB[0] = inA[0] ^ inB[1] ^ inB[2] ^ inB[7];
assign outA[1] = inA[1] ^ inB[0] ^ inB[3] ^ inB[6] ^ inB[7];
assign outB[1] = inA[1] ^ inB[0] ^ inB[1] ^ inB[3] ^ inB[6] ^ inB[7];
assign outA[2] = inA[2] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[6];
assign outB[2] = inA[2] ^ inB[0] ^ inB[1] ^ inB[2] ^ inB[4] ^ inB[6];
assign outA[3] = inA[3] ^ inB[0] ^ inB[5] ^ inB[7];
assign outB[3] = inA[3] ^ inB[0] ^ inB[3] ^ inB[5] ^ inB[7];
assign outA[4] = inA[4] ^ inB[0] ^ inB[1] ^ inB[6];
assign outB[4] = inA[4] ^ inB[0] ^ inB[1] ^ inB[4] ^ inB[6];
assign outA[5] = inA[5] ^ inB[0] ^ inB[7];
assign outB[5] = inA[5] ^ inB[0] ^ inB[5] ^ inB[7];
assign outA[6] = inA[6] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[6] ^ inB[7];
assign outB[6] = inA[6] ^ inB[2] ^ inB[3] ^ inB[4] ^ inB[5] ^ inB[7];
assign outA[7] = inA[7] ^ inB[2] ^ inB[4] ^ inB[6];
assign outB[7] = inA[7] ^ inB[2] ^ inB[4] ^ inB[6] ^ inB[7];

endmodule

