`include "aes_128.v"
`include "fft.v"
`include "bram.v"

module aes_gcm (
	input					clk,
	input					rst,
	input					enable,
	input		[127:0]		k,
	input		[95:0]		iv,
	input		[127:0]		p,
	input		[127:0]		a,
	input		[127:0]		len,
	output					valid,
	output					ready,
	output		[127:0]		c,
	output		[127:0]		t
);

reg			[8:0]		Fsm_Top; // Length
reg			[214:0]		Fsm_Shift; // Length
reg			[182:0]		Fsm_Init_Shift; // Length
reg			[31:0]		Counter;
reg			[37:0]		Counter_Shift; // Length
reg						Flag_1; // Counter >= 37 && Fsm <= 13
reg						Flag_2; // Fsm <= 22
reg						Flag_3; // Fsm >= 14 && Fsm <= 26
reg						Flag_4; // Counter >= 23
reg			[127:0]		H;
reg			[127:0]		H13;
reg			[127:0]		C0;
reg			[127:0]		Len;
reg			[127:0]		P01;	
reg			[127:0]		P02;	
reg			[127:0]		P03;	
reg			[127:0]		P04;	
reg			[127:0]		P05;	
reg			[127:0]		P06;	
reg			[127:0]		P07;	
reg			[127:0]		P08;	
reg			[127:0]		P09;	
reg			[127:0]		P10;	
reg			[127:0]		P11;	
reg			[127:0]		P12;	
reg			[127:0]		P13;	
reg			[127:0]		P14;	
reg			[127:0]		P15;	
reg			[127:0]		P16;	
reg			[127:0]		P17;	
reg			[127:0]		P18;	
reg			[127:0]		P19;	
reg			[127:0]		P20;	
reg			[127:0]		P21;
reg			[127:0]		C;
reg						Valid;
reg			[127:0]		InA;
reg			[127:0]		InB;
reg			[127:0]		T01;	
reg			[127:0]		T02;	
reg			[127:0]		T03;	
reg			[127:0]		T04;	
reg			[127:0]		T05;	
reg			[127:0]		T06;	
reg			[127:0]		T07;	
reg			[127:0]		T08;	
reg			[127:0]		T09;	
reg			[127:0]		T10;	
reg			[127:0]		T11;	
reg			[127:0]		T12;	
reg			[127:0]		T13;	
reg						Ready;
reg			[127:0]		T;
reg						Bram_Enable;
reg						Bram_Cmd;
reg			[127:0]		Bram_In_Rev;

wire		[127:0]		state;
wire		[127:0]		key;
wire		[127:0]		out;
wire		[127:0]		inA;
wire		[127:0]		inB;
wire		[127:0]		outC;
reg			[127:0]		bram_in;
wire		[127:0]		bram_out;

integer					i1, i2, i3;

assign key = k;
assign state = {iv, Counter};
assign c = C;
assign valid = Valid;
assign inA = InA;
assign inB = InB; 
assign ready = Ready;
assign t = T;

always @ (*) begin
	for (i1=0; i1<=127; i1=i1+1) begin
		bram_in[i1] <= Bram_In_Rev[127-i1];
	end
end

always @ (posedge clk) begin
	// Reseting and Pre-computation
	if (rst) begin
		Fsm_Top <= 0;
		Fsm_Shift <= 1;
		Fsm_Init_Shift <= 1;
		Counter <= 0;
		Counter_Shift <= 1;
		Flag_1 <= 0;
		Flag_2 <= 1;
		Flag_3 <= 0;
		Flag_4 <= 0;
		H <= 0;
		H13 <= 0;
		C0 <= 0;
		Len <= 0;
		P01 <= 0;
		P02 <= 0;
		P03 <= 0;
		P04 <= 0;
		P05 <= 0;
		P06 <= 0;
		P07 <= 0;
		P08 <= 0;
		P09 <= 0;
		P10 <= 0;
		P11 <= 0;
		P12 <= 0;
		P13 <= 0;
		P14 <= 0;
		P15 <= 0;
		P16 <= 0;
		P17 <= 0;
		P18 <= 0;
		P19 <= 0;
		P20 <= 0;
		P21 <= 0;
		C <= 0;
		Valid <= 0;
		InA <= 0;
		InB <= 0;
		T01 <= 0;
		T02 <= 0;
		T03 <= 0;
		T04 <= 0;
		T05 <= 0;
		T06 <= 0;
		T07 <= 0;
		T08 <= 0;
		T09 <= 0;
		T10 <= 0;
		T11 <= 0;
		T12 <= 0;
		T13 <= 0;
		Ready <= 0;
		T <= 0;
		Bram_Enable <= 0;
		Bram_Cmd <= 0;
		Bram_In_Rev <= 0;
	end
	else if (Fsm_Top == 0) begin
		Fsm_Init_Shift <= {Fsm_Init_Shift[181:0], 1'b0};
		if (Fsm_Init_Shift[0] == 1) begin
			Counter <= 1;
		end
		else if (Fsm_Init_Shift[21] == 1) begin
			Bram_Enable <= 1;
			Bram_Cmd <= 0;
			Bram_In_Rev <= out;
		end
		else if (Fsm_Init_Shift[22] == 1) begin
			C0 <= out;
			Bram_Enable <= 0;
		end
		else if (Fsm_Init_Shift[25] == 1) begin
			H <= bram_out;
			InA <= bram_out;
			InB <= bram_out;
		end
		else if (Fsm_Init_Shift[181] == 0 && Fsm_Init_Shift[182] == 0) begin
			InB <= outC;
		end
		else if (Fsm_Init_Shift[181] == 1) begin
			Fsm_Top <= 1;
			Counter <= 2;
			Counter_Shift <= 4;
			H13 <= outC;
			InA <= 0;
			InB <= 0;
			Ready <= 1;
		end
	end
	// Enabling Counter and Post-computation
	else if ((enable == 1) || (Fsm_Shift[0] == 0 && Fsm_Shift[214] == 0)) begin
		// Behavior of Fsm_Shift
		if (enable == 0) begin
			Fsm_Shift <= {Fsm_Shift[213:0], 1'b0};
		end
		else if (enable == 1) begin
			Fsm_Shift <= 2;
		end
		// Behavior of Counter
		Counter <= Counter + 1;
		// Behavior of Counter_Shift
		if (Counter < 37) begin
			Counter_Shift <= {Counter_Shift[36:0], 1'b0};
		end
		// Behavior of Flag_1
		if ((Counter_Shift[36] == 1 || Counter_Shift[37] == 1) && (Fsm_Shift[1] == 1 || Fsm_Shift[2] == 1 || Fsm_Shift[3] == 1 || Fsm_Shift[4] == 1 || Fsm_Shift[5] == 1 || Fsm_Shift[6] == 1 || Fsm_Shift[7] == 1 || Fsm_Shift[8] == 1 || Fsm_Shift[9] == 1 || Fsm_Shift[10] == 1 || Fsm_Shift[11] == 1 || Fsm_Shift[12] == 1)) begin
			Flag_1 <= 1;
		end
		else begin
			Flag_1 <= 0;
		end
		// Behavior of Flag_2
		if (Fsm_Shift[22] == 1) begin
			Flag_2 <= 0;
		end
		// Behavior of Flag_3
		if (Fsm_Shift[13] == 1) begin
			Flag_3 <= 1;
		end
		else if (Fsm_Shift[26] == 1) begin
			Flag_3 <= 0;
		end
		// Behavior of Flag_4
		if (Counter_Shift[22] == 1) begin
			Flag_4 <= 1;
		end
		// Behavior of P01~P21
		P01 <= p;
		P02 <= P01;
		P03 <= P02;
		P04 <= P03;
		P05 <= P04;
		P06 <= P05;
		P07 <= P06;
		P08 <= P07;
		P09 <= P08;
		P10 <= P09;
		P11 <= P10;
		P12 <= P11;
		P13 <= P12;
		P14 <= P13;
		P15 <= P14;
		P16 <= P15;
		P17 <= P16;
		P18 <= P17;
		P19 <= P18;
		P20 <= P19;
		P21 <= P20;
		// Behavior of C
		C <= P21 ^ out;
		// Behavior of Valid
		if (Flag_4 == 1 && Flag_2 == 1 && Fsm_Shift[22] == 0) begin
			Valid <= 1;
		end
		else begin
			Valid <= 0;
		end
		// Behavior of InA and InB
		if (Counter_Shift[37] == 0 && valid) begin
			InA <= bram_out;
			InB <= H13;
		end
		else if (Flag_1 == 1) begin
			InA <= bram_out ^ outC;
		end
		else if (Fsm_Shift[27] == 1) begin
			InA <= T13;
			InB <= H;
		end
		else begin
			InA <= outC ^ T12;
		end
		// Behavior of T01~T13
		if (Flag_3 == 1) begin
			T01 <= bram_out ^ outC;
			T02 <= T01;
			T03 <= T02;
			T04 <= T03;
			T05 <= T04;
			T06 <= T05;
			T07 <= T06;
			T08 <= T07;
			T09 <= T08;
			T10 <= T09;
			T11 <= T10;
			T12 <= T11;
			T13 <= T12;
		end
		else if (Fsm_Shift[52] == 1 || Fsm_Shift[65] == 1 || Fsm_Shift[78] == 1 || Fsm_Shift[91] == 1 || Fsm_Shift[104] == 1 || Fsm_Shift[117] == 1 || Fsm_Shift[130] == 1 || Fsm_Shift[143] == 1 || Fsm_Shift[156] == 1 || Fsm_Shift[169] == 1 || Fsm_Shift[182] == 1 || Fsm_Shift[195] == 1) begin
			T12 <= T11;
			T11 <= T10;
			T10 <= T09;
			T09 <= T08;
			T08 <= T07;
			T07 <= T06;
			T06 <= T05;
			T05 <= T04;
			T04 <= T03;
			T03 <= T02;
			T02 <= T01;
			T01 <= Len;
		end
		// Behavior of Ready
		if (enable == 1) begin
			Ready <= 0;
		end
		else if (Fsm_Shift[213] == 1) begin
			Ready <= 1;
		end
		// Behavior of T
		if (Fsm_Shift[213] == 1) begin
			for (i2=0; i2<=127; i2=i2+1) begin
				T[i2] <= bram_out[127-i2];
			end
		end
		// Behavior of Bram_Enable, Bram_Cmd and Bram_In_Rev
		if (Counter_Shift[23] == 1) begin
			Bram_Enable <= 1;
			Bram_Cmd <= 0;
			Bram_In_Rev <= a;
		end
		else if (Flag_2 == 1) begin
			Bram_In_Rev <= C;
		end
		else if (Fsm_Shift[47] == 1) begin
			Bram_Enable <= 1;
			Bram_In_Rev <= len;
		end
		else if (Fsm_Shift[48] == 1) begin
			Bram_Enable <= 0;
		end
		else if (Fsm_Shift[205] == 1) begin
			Bram_Enable <= 1;
			Bram_Cmd <= 0;
			Bram_In_Rev <= C0;
		end
		else if (Fsm_Shift[206] == 1) begin
			Bram_Enable <= 0;
		end
		else if (Fsm_Shift[209] == 1) begin
			Bram_Enable <= 1;
			Bram_Cmd <= 1;
			for (i3=0; i3<=127; i3=i3+1) begin
				Bram_In_Rev[i3] <= outC[127-i3] ^ bram_out[127-i3];
			end
		end
		else if (Fsm_Shift[210] == 1) begin
			Bram_Enable <= 0;
		end
		// Behavior of Len
		if (Fsm_Shift[51] == 1) begin
			Len <= bram_out;
		end
	end
end

aes_128 AES_128_0
	(.clk(clk), .state(state), .key(key), .out(out));

fft FFT_0
	(.clk(clk), .inA(inA), .inB(inB), .outC(outC));

bram BRAM_0
	(.clk(clk), .bram_enable(Bram_Enable), .bram_cmd(Bram_Cmd), .bram_in(bram_in), .bram_out(bram_out));

endmodule
