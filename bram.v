`include "bram_0x0_lo.v"
`include "bram_0x0_hi.v"
`include "bram_0x1_lo.v"
`include "bram_0x1_hi.v"
`include "bram_0x2_lo.v"
`include "bram_0x2_hi.v"
`include "bram_0x3_lo.v"
`include "bram_0x3_hi.v"
`include "bram_0x4_lo.v"
`include "bram_0x4_hi.v"
`include "bram_0x5_lo.v"
`include "bram_0x5_hi.v"
`include "bram_0x6_lo.v"
`include "bram_0x6_hi.v"
`include "bram_0x7_lo.v"
`include "bram_0x7_hi.v"
`include "bram_0x8_lo.v"
`include "bram_0x8_hi.v"
`include "bram_0x9_lo.v"
`include "bram_0x9_hi.v"
`include "bram_0xa_lo.v"
`include "bram_0xa_hi.v"
`include "bram_0xb_lo.v"
`include "bram_0xb_hi.v"
`include "bram_0xc_lo.v"
`include "bram_0xc_hi.v"
`include "bram_0xd_lo.v"
`include "bram_0xd_hi.v"
`include "bram_0xe_lo.v"
`include "bram_0xe_hi.v"
`include "bram_0xf_lo.v"
`include "bram_0xf_hi.v"

module bram (
	input					clk,
	input					bram_enable,
	input					bram_cmd,
	input		[127:0]		bram_in,
	output		[127:0]		bram_out
);

reg						Bram_Enable;
reg						Bram_Enable_Delay;
reg			[127:0]		Bram_Out;
reg			[63:0]		Odata_00;
reg			[63:0]		Odata_01;
reg			[63:0]		Odata_10;
reg			[63:0]		Odata_11;
reg			[63:0]		Odata_20;
reg			[63:0]		Odata_21;
reg			[63:0]		Odata_30;
reg			[63:0]		Odata_31;
reg			[63:0]		Odata_40;
reg			[63:0]		Odata_41;
reg			[63:0]		Odata_50;
reg			[63:0]		Odata_51;
reg			[63:0]		Odata_60;
reg			[63:0]		Odata_61;
reg			[63:0]		Odata_70;
reg			[63:0]		Odata_71;
reg			[63:0]		Odata_80;
reg			[63:0]		Odata_81;
reg			[63:0]		Odata_90;
reg			[63:0]		Odata_91;
reg			[63:0]		Odata_a0;
reg			[63:0]		Odata_a1;
reg			[63:0]		Odata_b0;
reg			[63:0]		Odata_b1;
reg			[63:0]		Odata_c0;
reg			[63:0]		Odata_c1;
reg			[63:0]		Odata_d0;
reg			[63:0]		Odata_d1;
reg			[63:0]		Odata_e0;
reg			[63:0]		Odata_e1;
reg			[63:0]		Odata_f0;
reg			[63:0]		Odata_f1;

wire		[8:0]		oaddr_00;
wire		[8:0]		oaddr_01;
wire		[8:0]		oaddr_10;
wire		[8:0]		oaddr_11;
wire		[8:0]		oaddr_20;
wire		[8:0]		oaddr_21;
wire		[8:0]		oaddr_30;
wire		[8:0]		oaddr_31;
wire		[8:0]		oaddr_40;
wire		[8:0]		oaddr_41;
wire		[8:0]		oaddr_50;
wire		[8:0]		oaddr_51;
wire		[8:0]		oaddr_60;
wire		[8:0]		oaddr_61;
wire		[8:0]		oaddr_70;
wire		[8:0]		oaddr_71;
wire		[8:0]		oaddr_80;
wire		[8:0]		oaddr_81;
wire		[8:0]		oaddr_90;
wire		[8:0]		oaddr_91;
wire		[8:0]		oaddr_a0;
wire		[8:0]		oaddr_a1;
wire		[8:0]		oaddr_b0;
wire		[8:0]		oaddr_b1;
wire		[8:0]		oaddr_c0;
wire		[8:0]		oaddr_c1;
wire		[8:0]		oaddr_d0;
wire		[8:0]		oaddr_d1;
wire		[8:0]		oaddr_e0;
wire		[8:0]		oaddr_e1;
wire		[8:0]		oaddr_f0;
wire		[8:0]		oaddr_f1;
wire		[63:0]		odata_00;
wire		[63:0]		odata_01;
wire		[63:0]		odata_10;
wire		[63:0]		odata_11;
wire		[63:0]		odata_20;
wire		[63:0]		odata_21;
wire		[63:0]		odata_30;
wire		[63:0]		odata_31;
wire		[63:0]		odata_40;
wire		[63:0]		odata_41;
wire		[63:0]		odata_50;
wire		[63:0]		odata_51;
wire		[63:0]		odata_60;
wire		[63:0]		odata_61;
wire		[63:0]		odata_70;
wire		[63:0]		odata_71;
wire		[63:0]		odata_80;
wire		[63:0]		odata_81;
wire		[63:0]		odata_90;
wire		[63:0]		odata_91;
wire		[63:0]		odata_a0;
wire		[63:0]		odata_a1;
wire		[63:0]		odata_b0;
wire		[63:0]		odata_b1;
wire		[63:0]		odata_c0;
wire		[63:0]		odata_c1;
wire		[63:0]		odata_d0;
wire		[63:0]		odata_d1;
wire		[63:0]		odata_e0;
wire		[63:0]		odata_e1;
wire		[63:0]		odata_f0;
wire		[63:0]		odata_f1;

assign oaddr_00 = {bram_cmd, bram_in[7:0]};
assign oaddr_01 = {bram_cmd, bram_in[7:0]};
assign oaddr_10 = {bram_cmd, bram_in[15:8]};
assign oaddr_11 = {bram_cmd, bram_in[15:8]};
assign oaddr_20 = {bram_cmd, bram_in[23:16]};
assign oaddr_21 = {bram_cmd, bram_in[23:16]};
assign oaddr_30 = {bram_cmd, bram_in[31:24]};
assign oaddr_31 = {bram_cmd, bram_in[31:24]};
assign oaddr_40 = {bram_cmd, bram_in[39:32]};
assign oaddr_41 = {bram_cmd, bram_in[39:32]};
assign oaddr_50 = {bram_cmd, bram_in[47:40]};
assign oaddr_51 = {bram_cmd, bram_in[47:40]};
assign oaddr_60 = {bram_cmd, bram_in[55:48]};
assign oaddr_61 = {bram_cmd, bram_in[55:48]};
assign oaddr_70 = {bram_cmd, bram_in[63:56]};
assign oaddr_71 = {bram_cmd, bram_in[63:56]};
assign oaddr_80 = {bram_cmd, bram_in[71:64]};
assign oaddr_81 = {bram_cmd, bram_in[71:64]};
assign oaddr_90 = {bram_cmd, bram_in[79:72]};
assign oaddr_91 = {bram_cmd, bram_in[79:72]};
assign oaddr_a0 = {bram_cmd, bram_in[87:80]};
assign oaddr_a1 = {bram_cmd, bram_in[87:80]};
assign oaddr_b0 = {bram_cmd, bram_in[95:88]};
assign oaddr_b1 = {bram_cmd, bram_in[95:88]};
assign oaddr_c0 = {bram_cmd, bram_in[103:96]};
assign oaddr_c1 = {bram_cmd, bram_in[103:96]};
assign oaddr_d0 = {bram_cmd, bram_in[111:104]};
assign oaddr_d1 = {bram_cmd, bram_in[111:104]};
assign oaddr_e0 = {bram_cmd, bram_in[119:112]};
assign oaddr_e1 = {bram_cmd, bram_in[119:112]};
assign oaddr_f0 = {bram_cmd, bram_in[127:120]};
assign oaddr_f1 = {bram_cmd, bram_in[127:120]};
assign bram_out = Bram_Out;

always @ (posedge clk) begin
	Bram_Enable <= bram_enable;
	Bram_Enable_Delay <= Bram_Enable;
end

always @ (posedge clk) begin
	if (Bram_Enable_Delay == 0) begin
		Bram_Out <= 0;
	end
	else begin
	Bram_Out[63:0] <= Odata_00 ^ Odata_10 ^ Odata_20 ^ Odata_30 ^ Odata_40 ^ Odata_50 ^ Odata_60 ^ Odata_70 ^ Odata_80 ^ Odata_90 ^ Odata_a0 ^ Odata_b0 ^ Odata_c0 ^ Odata_d0 ^ Odata_e0 ^ Odata_f0;
	Bram_Out[127:64] <= Odata_01 ^ Odata_11 ^ Odata_21 ^ Odata_31 ^ Odata_41 ^ Odata_51 ^ Odata_61 ^ Odata_71 ^ Odata_81 ^ Odata_91 ^ Odata_a1 ^ Odata_b1 ^ Odata_c1 ^ Odata_d1 ^ Odata_e1 ^ Odata_f1;
	end
end

always @ (posedge clk) begin
	Odata_00 <= odata_00;
	Odata_01 <= odata_01;
	Odata_10 <= odata_10;
	Odata_11 <= odata_11;
	Odata_20 <= odata_20;
	Odata_21 <= odata_21;
	Odata_30 <= odata_30;
	Odata_31 <= odata_31;
	Odata_40 <= odata_40;
	Odata_41 <= odata_41;
	Odata_50 <= odata_50;
	Odata_51 <= odata_51;
	Odata_60 <= odata_60;
	Odata_61 <= odata_61;
	Odata_70 <= odata_70;
	Odata_71 <= odata_71;
	Odata_80 <= odata_80;
	Odata_81 <= odata_81;
	Odata_90 <= odata_90;
	Odata_91 <= odata_91;
	Odata_a0 <= odata_a0;
	Odata_a1 <= odata_a1;
	Odata_b0 <= odata_b0;
	Odata_b1 <= odata_b1;
	Odata_c0 <= odata_c0;
	Odata_c1 <= odata_c1;
	Odata_d0 <= odata_d0;
	Odata_d1 <= odata_d1;
	Odata_e0 <= odata_e0;
	Odata_e1 <= odata_e1;
	Odata_f0 <= odata_f0;
	Odata_f1 <= odata_f1;
end

bram_0x0_lo BRAM_00
	(.clk(clk), .oaddr(oaddr_00), .odata(odata_00));

bram_0x0_hi BRAM_01
	(.clk(clk), .oaddr(oaddr_01), .odata(odata_01));

bram_0x1_lo BRAM_10
	(.clk(clk), .oaddr(oaddr_10), .odata(odata_10));

bram_0x1_hi BRAM_11
	(.clk(clk), .oaddr(oaddr_11), .odata(odata_11));

bram_0x2_lo BRAM_20
	(.clk(clk), .oaddr(oaddr_20), .odata(odata_20));

bram_0x2_hi BRAM_21
	(.clk(clk), .oaddr(oaddr_21), .odata(odata_21));

bram_0x3_lo BRAM_30
	(.clk(clk), .oaddr(oaddr_30), .odata(odata_30));

bram_0x3_hi BRAM_31
	(.clk(clk), .oaddr(oaddr_31), .odata(odata_31));

bram_0x4_lo BRAM_40
	(.clk(clk), .oaddr(oaddr_40), .odata(odata_40));

bram_0x4_hi BRAM_41
	(.clk(clk), .oaddr(oaddr_41), .odata(odata_41));

bram_0x5_lo BRAM_50
	(.clk(clk), .oaddr(oaddr_50), .odata(odata_50));

bram_0x5_hi BRAM_51
	(.clk(clk), .oaddr(oaddr_51), .odata(odata_51));

bram_0x6_lo BRAM_60
	(.clk(clk), .oaddr(oaddr_60), .odata(odata_60));

bram_0x6_hi BRAM_61
	(.clk(clk), .oaddr(oaddr_61), .odata(odata_61));

bram_0x7_lo BRAM_70
	(.clk(clk), .oaddr(oaddr_70), .odata(odata_70));

bram_0x7_hi BRAM_71
	(.clk(clk), .oaddr(oaddr_71), .odata(odata_71));

bram_0x8_lo BRAM_80
	(.clk(clk), .oaddr(oaddr_80), .odata(odata_80));

bram_0x8_hi BRAM_81
	(.clk(clk), .oaddr(oaddr_81), .odata(odata_81));

bram_0x9_lo BRAM_90
	(.clk(clk), .oaddr(oaddr_90), .odata(odata_90));

bram_0x9_hi BRAM_91
	(.clk(clk), .oaddr(oaddr_91), .odata(odata_91));

bram_0xa_lo BRAM_A0
	(.clk(clk), .oaddr(oaddr_a0), .odata(odata_a0));

bram_0xa_hi BRAM_A1
	(.clk(clk), .oaddr(oaddr_a1), .odata(odata_a1));

bram_0xb_lo BRAM_B0
	(.clk(clk), .oaddr(oaddr_b0), .odata(odata_b0));

bram_0xb_hi BRAM_B1
	(.clk(clk), .oaddr(oaddr_b1), .odata(odata_b1));

bram_0xc_lo BRAM_C0
	(.clk(clk), .oaddr(oaddr_c0), .odata(odata_c0));

bram_0xc_hi BRAM_C1
	(.clk(clk), .oaddr(oaddr_c1), .odata(odata_c1));

bram_0xd_lo BRAM_D0
	(.clk(clk), .oaddr(oaddr_d0), .odata(odata_d0));

bram_0xd_hi BRAM_D1
	(.clk(clk), .oaddr(oaddr_d1), .odata(odata_d1));

bram_0xe_lo BRAM_E0
	(.clk(clk), .oaddr(oaddr_e0), .odata(odata_e0));

bram_0xe_hi BRAM_E1
	(.clk(clk), .oaddr(oaddr_e1), .odata(odata_e1));

bram_0xf_lo BRAM_F0
	(.clk(clk), .oaddr(oaddr_f0), .odata(odata_f0));

bram_0xf_hi BRAM_F1
	(.clk(clk), .oaddr(oaddr_f1), .odata(odata_f1));

endmodule
