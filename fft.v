`include "bs_cvt.v"
`include "butterfly.v"
`include "gf.v"
`include "inv_butterfly.v"
`include "ivs_bs_cvt.v"
`include "reduce.v"

module fft (
	clk,
	inA,
	inB,
	outC
);

input						clk;
input		[127:0]			inA;
input		[127:0]			inB;
output		[127:0]			outC;

wire		[255:0]			w0;
wire		[255:0]			w1;
wire		[255:0]			w2;
wire		[255:0]			w3;

bs_cvt FFT_FOWARD_0 (.clk(clk), .poly_in(inA), .poly_out(w0));
bs_cvt FFT_FOWARD_1 (.clk(clk), .poly_in(inB), .poly_out(w1));
gf256_mul GF256_MUL_00 (.inA(w0[  7:  0]), .inB(w1[  7:  0]), .outC(w2[  7:  0]));
gf256_mul GF256_MUL_01 (.inA(w0[ 15:  8]), .inB(w1[ 15:  8]), .outC(w2[ 15:  8]));
gf256_mul GF256_MUL_02 (.inA(w0[ 23: 16]), .inB(w1[ 23: 16]), .outC(w2[ 23: 16]));
gf256_mul GF256_MUL_03 (.inA(w0[ 31: 24]), .inB(w1[ 31: 24]), .outC(w2[ 31: 24]));
gf256_mul GF256_MUL_04 (.inA(w0[ 39: 32]), .inB(w1[ 39: 32]), .outC(w2[ 39: 32]));
gf256_mul GF256_MUL_05 (.inA(w0[ 47: 40]), .inB(w1[ 47: 40]), .outC(w2[ 47: 40]));
gf256_mul GF256_MUL_06 (.inA(w0[ 55: 48]), .inB(w1[ 55: 48]), .outC(w2[ 55: 48]));
gf256_mul GF256_MUL_07 (.inA(w0[ 63: 56]), .inB(w1[ 63: 56]), .outC(w2[ 63: 56]));
gf256_mul GF256_MUL_08 (.inA(w0[ 71: 64]), .inB(w1[ 71: 64]), .outC(w2[ 71: 64]));
gf256_mul GF256_MUL_09 (.inA(w0[ 79: 72]), .inB(w1[ 79: 72]), .outC(w2[ 79: 72]));
gf256_mul GF256_MUL_10 (.inA(w0[ 87: 80]), .inB(w1[ 87: 80]), .outC(w2[ 87: 80]));
gf256_mul GF256_MUL_11 (.inA(w0[ 95: 88]), .inB(w1[ 95: 88]), .outC(w2[ 95: 88]));
gf256_mul GF256_MUL_12 (.inA(w0[103: 96]), .inB(w1[103: 96]), .outC(w2[103: 96]));
gf256_mul GF256_MUL_13 (.inA(w0[111:104]), .inB(w1[111:104]), .outC(w2[111:104]));
gf256_mul GF256_MUL_14 (.inA(w0[119:112]), .inB(w1[119:112]), .outC(w2[119:112]));
gf256_mul GF256_MUL_15 (.inA(w0[127:120]), .inB(w1[127:120]), .outC(w2[127:120]));
gf256_mul GF256_MUL_16 (.inA(w0[135:128]), .inB(w1[135:128]), .outC(w2[135:128]));
gf256_mul GF256_MUL_17 (.inA(w0[143:136]), .inB(w1[143:136]), .outC(w2[143:136]));
gf256_mul GF256_MUL_18 (.inA(w0[151:144]), .inB(w1[151:144]), .outC(w2[151:144]));
gf256_mul GF256_MUL_19 (.inA(w0[159:152]), .inB(w1[159:152]), .outC(w2[159:152]));
gf256_mul GF256_MUL_20 (.inA(w0[167:160]), .inB(w1[167:160]), .outC(w2[167:160]));
gf256_mul GF256_MUL_21 (.inA(w0[175:168]), .inB(w1[175:168]), .outC(w2[175:168]));
gf256_mul GF256_MUL_22 (.inA(w0[183:176]), .inB(w1[183:176]), .outC(w2[183:176]));
gf256_mul GF256_MUL_23 (.inA(w0[191:184]), .inB(w1[191:184]), .outC(w2[191:184]));
gf256_mul GF256_MUL_24 (.inA(w0[199:192]), .inB(w1[199:192]), .outC(w2[199:192]));
gf256_mul GF256_MUL_25 (.inA(w0[207:200]), .inB(w1[207:200]), .outC(w2[207:200]));
gf256_mul GF256_MUL_26 (.inA(w0[215:208]), .inB(w1[215:208]), .outC(w2[215:208]));
gf256_mul GF256_MUL_27 (.inA(w0[223:216]), .inB(w1[223:216]), .outC(w2[223:216]));
gf256_mul GF256_MUL_28 (.inA(w0[231:224]), .inB(w1[231:224]), .outC(w2[231:224]));
gf256_mul GF256_MUL_29 (.inA(w0[239:232]), .inB(w1[239:232]), .outC(w2[239:232]));
gf256_mul GF256_MUL_30 (.inA(w0[247:240]), .inB(w1[247:240]), .outC(w2[247:240]));
gf256_mul GF256_MUL_31 (.inA(w0[255:248]), .inB(w1[255:248]), .outC(w2[255:248]));
ivs_bs_cvt FFT_BACKWARD (.clk(clk), .poly_in(w2), .poly_out(w3));
reduce REDUCE (.clk(clk), .poly_in(w3), .poly_out(outC));

endmodule
