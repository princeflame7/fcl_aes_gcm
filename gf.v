module gf4_add (
	inA,
	inB,
	outC
);

input		[1:0]		inA;
input		[1:0]		inB;
output		[1:0]		outC;

assign outC[0] = inA[0] ^ inB[0];
assign outC[1] = inA[1] ^ inB[1];

//LUT6_2 #(
//	.INIT(64'hXXXX_5A5A_XXXX_33CC)
//) LUT6_2_inst (
//	.O6(outC[0]),
//	.O5(outC[1]),
//	.I0(inB[0]),
//	.I1(inB[1]),
//	.I2(inA[0]),
//	.I3(inA[1]),
//	.I4(1'b0),
//	.I5(1'b1)
//);

endmodule

module gf4_mul (
	inA,
	inB,
	outC
);

input		[1:0]		inA;
input		[1:0]		inB;
output		[1:0]		outC;

assign outC = ({inA, inB} == 4'b0000) ? 2'b00 :
              ({inA, inB} == 4'b0001) ? 2'b00 :
			  ({inA, inB} == 4'b0010) ? 2'b00 :
			  ({inA, inB} == 4'b0011) ? 2'b00 :
			  ({inA, inB} == 4'b0100) ? 2'b00 :
              ({inA, inB} == 4'b0101) ? 2'b01 :
			  ({inA, inB} == 4'b0110) ? 2'b10 :
			  ({inA, inB} == 4'b0111) ? 2'b11 :
			  ({inA, inB} == 4'b1000) ? 2'b00 :
              ({inA, inB} == 4'b1001) ? 2'b10 :
			  ({inA, inB} == 4'b1010) ? 2'b11 :
			  ({inA, inB} == 4'b1011) ? 2'b01 :
			  ({inA, inB} == 4'b1100) ? 2'b00 :
              ({inA, inB} == 4'b1101) ? 2'b11 :
			  ({inA, inB} == 4'b1110) ? 2'b01 :
			  ({inA, inB} == 4'b1111) ? 2'b10 : 2'b00;

//LUT6_2 #(
//	.INIT(64'hXXXX_6CA0_XXXX_A6C0)
//) LUT6_2_inst (
//	.O6(outC[0]),
//	.O5(outC[1]),
//	.I0(inB[0]),
//	.I1(inB[1]),
//	.I2(inA[0]),
//	.I3(inA[1]),
//	.I4(1'b0),
//	.I5(1'b1)
//);

endmodule

module gf4_mul_10 (
	inA,
	inB,
	outC
);

input		[1:0]		inA;
input		[1:0]		inB;
output		[1:0]		outC;

assign outC = ({inA, inB} == 4'b0000) ? 2'b00 :
              ({inA, inB} == 4'b0001) ? 2'b00 :
			  ({inA, inB} == 4'b0010) ? 2'b00 :
			  ({inA, inB} == 4'b0011) ? 2'b00 :
			  ({inA, inB} == 4'b0100) ? 2'b00 :
              ({inA, inB} == 4'b0101) ? 2'b10 :
			  ({inA, inB} == 4'b0110) ? 2'b11 :
			  ({inA, inB} == 4'b0111) ? 2'b01 :
			  ({inA, inB} == 4'b1000) ? 2'b00 :
              ({inA, inB} == 4'b1001) ? 2'b11 :
			  ({inA, inB} == 4'b1010) ? 2'b01 :
			  ({inA, inB} == 4'b1011) ? 2'b10 :
			  ({inA, inB} == 4'b1100) ? 2'b00 :
              ({inA, inB} == 4'b1101) ? 2'b01 :
			  ({inA, inB} == 4'b1110) ? 2'b10 :
			  ({inA, inB} == 4'b1111) ? 2'b11 : 2'b00;

//LUT6_2 #(
//	.INIT(64'hXXXX_A6C0_XXXX_CA60)
//) LUT6_2_inst (
//	.O6(outC[0]),
//	.O5(outC[1]),
//	.I0(inB[0]),
//	.I1(inB[1]),
//	.I2(inA[0]),
//	.I3(inA[1]),
//	.I4(1'b0),
//	.I5(1'b1)
//);

endmodule

module gf4_mul_11 (
	inA,
	inB,
	outC
);

input		[1:0]		inA;
input		[1:0]		inB;
output		[1:0]		outC;

assign outC = ({inA, inB} == 4'b0000) ? 2'b00 :
              ({inA, inB} == 4'b0001) ? 2'b00 :
			  ({inA, inB} == 4'b0010) ? 2'b00 :
			  ({inA, inB} == 4'b0011) ? 2'b00 :
			  ({inA, inB} == 4'b0100) ? 2'b00 :
              ({inA, inB} == 4'b0101) ? 2'b11 :
			  ({inA, inB} == 4'b0110) ? 2'b01 :
			  ({inA, inB} == 4'b0111) ? 2'b10 :
			  ({inA, inB} == 4'b1000) ? 2'b00 :
              ({inA, inB} == 4'b1001) ? 2'b01 :
			  ({inA, inB} == 4'b1010) ? 2'b10 :
			  ({inA, inB} == 4'b1011) ? 2'b11 :
			  ({inA, inB} == 4'b1100) ? 2'b00 :
              ({inA, inB} == 4'b1101) ? 2'b10 :
			  ({inA, inB} == 4'b1110) ? 2'b11 :
			  ({inA, inB} == 4'b1111) ? 2'b01 : 2'b00;

//LUT6_2 #(
//	.INIT(64'hXXXX_CA60_XXXX_6CA0)
//) LUT6_2_inst (
//	.O6(outC[0]),
//	.O5(outC[1]),
//	.I0(inB[0]),
//	.I1(inB[1]),
//	.I2(inA[0]),
//	.I3(inA[1]),
//	.I4(1'b0),
//	.I5(1'b1)
//);

endmodule

module gf16_add (
	inA,
	inB,
	outC
);

input		[3:0]		inA;
input		[3:0]		inB;
output		[3:0]		outC;

gf4_add GF4_ADD_0 (inA[1:0], inB[1:0], outC[1:0]);
gf4_add GF4_ADD_1 (inA[3:2], inB[3:2], outC[3:2]);

endmodule

module gf16_mul (
	inA,
	inB,
	outC
);

input		[3:0]		inA;
input		[3:0]		inB;
output		[3:0]		outC;
wire		[1:0]		a0;
wire		[1:0]		a1;
wire		[1:0]		b0;
wire		[1:0]		b1;
wire		[1:0]		c0;
wire		[1:0]		c1;
wire		[1:0]		w0;
wire		[1:0]		w1;
wire		[1:0]		w2;
wire		[1:0]		w3;
wire		[1:0]		w4;

assign {a1, a0} = inA;
assign {b1, b0} = inB;
assign outC = {c1, c0};

gf4_add GF4_ADD_0 (.inA(a0), .inB(a1), .outC(w0));
gf4_add GF4_ADD_1 (.inA(b0), .inB(b1), .outC(w1));
gf4_add GF4_ADD_2 (.inA(w2), .inB(w4), .outC(c1));
gf4_add GF4_ADD_3 (.inA(w2), .inB(w3), .outC(c0));
gf4_mul GF4_MUL_0 (.inA(a0), .inB(b0), .outC(w2));
gf4_mul GF4_MUL_1 (.inA(w0), .inB(w1), .outC(w4));
gf4_mul_10 GF4_MUL_10 (.inA(a1), .inB(b1), .outC(w3));

endmodule

module gf16_mul_1000 (
	inA,
	inB,
	outC
);

input		[3:0]		inA;
input		[3:0]		inB;
output		[3:0]		outC;
wire		[1:0]		a0;
wire		[1:0]		a1;
wire		[1:0]		b0;
wire		[1:0]		b1;
wire		[1:0]		c0;
wire		[1:0]		c1;
wire		[1:0]		w0;
wire		[1:0]		w1;
wire		[1:0]		w2;
wire		[1:0]		w3;
wire		[1:0]		w4;
wire		[1:0]		w5;

assign {a1, a0} = inA;
assign {b1, b0} = inB;
assign outC = {c1, c0};

gf4_add GF4_ADD_0 (.inA(a0), .inB(a1), .outC(w0));
gf4_add GF4_ADD_1 (.inA(b0), .inB(b1), .outC(w1));
gf4_add GF4_ADD_2 (.inA(w3), .inB(w4), .outC(c1));
gf4_add GF4_ADD_3 (.inA(w2), .inB(w5), .outC(c0));
gf4_mul_10 GF4_MUL_10 (.inA(w0), .inB(w1), .outC(w4));
gf4_mul_11 GF4_MUL_11_0 (.inA(a0), .inB(b0), .outC(w2));
gf4_mul_11 GF4_MUL_11_1 (.inA(a1), .inB(b1), .outC(w3));
gf4_mul_11 GF4_MUL_11_2 (.inA(w0), .inB(w1), .outC(w5));

endmodule

module gf256_add (
	inA,
	inB,
	outC
);

input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outC;

gf16_add GF16_ADD_0 (inA[3:0], inB[3:0], outC[3:0]);
gf16_add GF16_ADD_1 (inA[7:4], inB[7:4], outC[7:4]);

endmodule

module gf256_mul (
	inA,
	inB,
	outC
);

input		[7:0]		inA;
input		[7:0]		inB;
output		[7:0]		outC;
wire		[3:0]		a0;
wire		[3:0]		a1;
wire		[3:0]		b0;
wire		[3:0]		b1;
wire		[3:0]		c0;
wire		[3:0]		c1;
wire		[3:0]		w0;
wire		[3:0]		w1;
wire		[3:0]		w2;
wire		[3:0]		w3;
wire		[3:0]		w4;

assign {a1, a0} = inA;
assign {b1, b0} = inB;
assign outC = {c1, c0};

gf16_add GF16_ADD_0 (.inA(a0), .inB(a1), .outC(w0));
gf16_add GF16_ADD_1 (.inA(b0), .inB(b1), .outC(w1));
gf16_add GF16_ADD_2 (.inA(w2), .inB(w4), .outC(c1));
gf16_add GF16_ADD_3 (.inA(w2), .inB(w3), .outC(c0));
gf16_mul GF16_MUL_0 (.inA(a0), .inB(b0), .outC(w2));
gf16_mul GF16_MUL_1 (.inA(w0), .inB(w1), .outC(w4));
gf16_mul_1000 GF16_MUL_1000 (.inA(a1), .inB(b1), .outC(w3));

endmodule