module reduce (
	clk,
	poly_in,
	poly_out
);

input					clk;
input		[255:0]		poly_in;
output		[127:0]		poly_out;

assign poly_out[  7:  0] = poly_in[  7:  0] ^ { poly_in[131] ^ poly_in[135], poly_in[130] ^ poly_in[134], poly_in[129] ^ poly_in[133], poly_in[128] ^ poly_in[132], poly_in[132] ^ poly_in[133] ^ poly_in[134] ^ poly_in[135], poly_in[133] ^ poly_in[135], poly_in[134], poly_in[134] ^ poly_in[135] } ^ { poly_in[219] ^ poly_in[223], poly_in[218] ^ poly_in[222], poly_in[217] ^ poly_in[221], poly_in[216] ^ poly_in[220], poly_in[220] ^ poly_in[221] ^ poly_in[222] ^ poly_in[223], poly_in[221] ^ poly_in[223], poly_in[222], poly_in[222] ^ poly_in[223] } ^ { poly_in[235] ^ poly_in[239], poly_in[234] ^ poly_in[238], poly_in[233] ^ poly_in[237], poly_in[232] ^ poly_in[236], poly_in[236] ^ poly_in[237] ^ poly_in[238] ^ poly_in[239], poly_in[237] ^ poly_in[239], poly_in[238], poly_in[238] ^ poly_in[239] };

assign poly_out[ 15:  8] = poly_in[ 15:  8] ^ { poly_in[139] ^ poly_in[143], poly_in[138] ^ poly_in[142], poly_in[137] ^ poly_in[141], poly_in[136] ^ poly_in[140], poly_in[140] ^ poly_in[141] ^ poly_in[142] ^ poly_in[143], poly_in[141] ^ poly_in[143], poly_in[142], poly_in[142] ^ poly_in[143] } ^ { poly_in[227] ^ poly_in[231], poly_in[226] ^ poly_in[230], poly_in[225] ^ poly_in[229], poly_in[224] ^ poly_in[228], poly_in[228] ^ poly_in[229] ^ poly_in[230] ^ poly_in[231], poly_in[229] ^ poly_in[231], poly_in[230], poly_in[230] ^ poly_in[231] } ^ { poly_in[243] ^ poly_in[247], poly_in[242] ^ poly_in[246], poly_in[241] ^ poly_in[245], poly_in[240] ^ poly_in[244], poly_in[244] ^ poly_in[245] ^ poly_in[246] ^ poly_in[247], poly_in[245] ^ poly_in[247], poly_in[246], poly_in[246] ^ poly_in[247] };

assign poly_out[ 23: 16] = poly_in[ 23: 16] ^ { poly_in[147] ^ poly_in[151], poly_in[146] ^ poly_in[150], poly_in[145] ^ poly_in[149], poly_in[144] ^ poly_in[148], poly_in[148] ^ poly_in[149] ^ poly_in[150] ^ poly_in[151], poly_in[149] ^ poly_in[151], poly_in[150], poly_in[150] ^ poly_in[151] } ^ { poly_in[235] ^ poly_in[239], poly_in[234] ^ poly_in[238], poly_in[233] ^ poly_in[237], poly_in[232] ^ poly_in[236], poly_in[236] ^ poly_in[237] ^ poly_in[238] ^ poly_in[239], poly_in[237] ^ poly_in[239], poly_in[238], poly_in[238] ^ poly_in[239] } ^ { poly_in[251] ^ poly_in[255], poly_in[250] ^ poly_in[254], poly_in[249] ^ poly_in[253], poly_in[248] ^ poly_in[252], poly_in[252] ^ poly_in[253] ^ poly_in[254] ^ poly_in[255], poly_in[253] ^ poly_in[255], poly_in[254], poly_in[254] ^ poly_in[255] };

assign poly_out[ 31: 24] = poly_in[ 31: 24] ^ poly_in[135:128] ^ { poly_in[155] ^ poly_in[159], poly_in[154] ^ poly_in[158], poly_in[153] ^ poly_in[157], poly_in[152] ^ poly_in[156], poly_in[156] ^ poly_in[157] ^ poly_in[158] ^ poly_in[159], poly_in[157] ^ poly_in[159], poly_in[158], poly_in[158] ^ poly_in[159] } ^ poly_in[223:216] ^ poly_in[239:232] ^ { poly_in[243] ^ poly_in[247], poly_in[242] ^ poly_in[246], poly_in[241] ^ poly_in[245], poly_in[240] ^ poly_in[244], poly_in[244] ^ poly_in[245] ^ poly_in[246] ^ poly_in[247], poly_in[245] ^ poly_in[247], poly_in[246], poly_in[246] ^ poly_in[247] };

assign poly_out[ 39: 32] = poly_in[ 39: 32] ^ poly_in[143:136] ^ { poly_in[163] ^ poly_in[167], poly_in[162] ^ poly_in[166], poly_in[161] ^ poly_in[165], poly_in[160] ^ poly_in[164], poly_in[164] ^ poly_in[165] ^ poly_in[166] ^ poly_in[167], poly_in[165] ^ poly_in[167], poly_in[166], poly_in[166] ^ poly_in[167] } ^ poly_in[231:224] ^ poly_in[247:240] ^ { poly_in[251] ^ poly_in[255], poly_in[250] ^ poly_in[254], poly_in[249] ^ poly_in[253], poly_in[248] ^ poly_in[252], poly_in[252] ^ poly_in[253] ^ poly_in[254] ^ poly_in[255], poly_in[253] ^ poly_in[255], poly_in[254], poly_in[254] ^ poly_in[255] };

assign poly_out[ 47: 40] = poly_in[ 47: 40] ^ poly_in[135:128] ^ poly_in[151:144] ^ { poly_in[171] ^ poly_in[175], poly_in[170] ^ poly_in[174], poly_in[169] ^ poly_in[173], poly_in[168] ^ poly_in[172], poly_in[172] ^ poly_in[173] ^ poly_in[174] ^ poly_in[175], poly_in[173] ^ poly_in[175], poly_in[174], poly_in[174] ^ poly_in[175] } ^ poly_in[223:216] ^ poly_in[255:248];

assign poly_out[ 55: 48] = poly_in[ 55: 48] ^ poly_in[143:136] ^ poly_in[159:152] ^ { poly_in[179] ^ poly_in[183], poly_in[178] ^ poly_in[182], poly_in[177] ^ poly_in[181], poly_in[176] ^ poly_in[180], poly_in[180] ^ poly_in[181] ^ poly_in[182] ^ poly_in[183], poly_in[181] ^ poly_in[183], poly_in[182], poly_in[182] ^ poly_in[183] } ^ poly_in[231:224];

assign poly_out[ 63: 56] = poly_in[ 63: 56] ^ poly_in[151:144] ^ poly_in[167:160] ^ { poly_in[187] ^ poly_in[191], poly_in[186] ^ poly_in[190], poly_in[185] ^ poly_in[189], poly_in[184] ^ poly_in[188], poly_in[188] ^ poly_in[189] ^ poly_in[190] ^ poly_in[191], poly_in[189] ^ poly_in[191], poly_in[190], poly_in[190] ^ poly_in[191] } ^ poly_in[239:232];

assign poly_out[ 71: 64] = poly_in[ 71: 64] ^ poly_in[159:152] ^ poly_in[175:168] ^ { poly_in[195] ^ poly_in[199], poly_in[194] ^ poly_in[198], poly_in[193] ^ poly_in[197], poly_in[192] ^ poly_in[196], poly_in[196] ^ poly_in[197] ^ poly_in[198] ^ poly_in[199], poly_in[197] ^ poly_in[199], poly_in[198], poly_in[198] ^ poly_in[199] } ^ poly_in[247:240];

assign poly_out[ 79: 72] = poly_in[ 79: 72] ^ poly_in[167:160] ^ poly_in[183:176] ^ { poly_in[203] ^ poly_in[207], poly_in[202] ^ poly_in[206], poly_in[201] ^ poly_in[205], poly_in[200] ^ poly_in[204], poly_in[204] ^ poly_in[205] ^ poly_in[206] ^ poly_in[207], poly_in[205] ^ poly_in[207], poly_in[206], poly_in[206] ^ poly_in[207] } ^ poly_in[255:248];

assign poly_out[ 87: 80] = poly_in[ 87: 80] ^ poly_in[175:168] ^ poly_in[191:184] ^ { poly_in[211] ^ poly_in[215], poly_in[210] ^ poly_in[214], poly_in[209] ^ poly_in[213], poly_in[208] ^ poly_in[212], poly_in[212] ^ poly_in[213] ^ poly_in[214] ^ poly_in[215], poly_in[213] ^ poly_in[215], poly_in[214], poly_in[214] ^ poly_in[215] };

assign poly_out[ 95: 88] = poly_in[ 95: 88] ^ poly_in[183:176] ^ poly_in[199:192] ^ { poly_in[219] ^ poly_in[223], poly_in[218] ^ poly_in[222], poly_in[217] ^ poly_in[221], poly_in[216] ^ poly_in[220], poly_in[220] ^ poly_in[221] ^ poly_in[222] ^ poly_in[223], poly_in[221] ^ poly_in[223], poly_in[222], poly_in[222] ^ poly_in[223] };

assign poly_out[103: 96] = poly_in[103: 96] ^ poly_in[191:184] ^ poly_in[207:200] ^ { poly_in[227] ^ poly_in[231], poly_in[226] ^ poly_in[230], poly_in[225] ^ poly_in[229], poly_in[224] ^ poly_in[228], poly_in[228] ^ poly_in[229] ^ poly_in[230] ^ poly_in[231], poly_in[229] ^ poly_in[231], poly_in[230], poly_in[230] ^ poly_in[231] };

assign poly_out[111:104] = poly_in[111:104] ^ poly_in[199:192] ^ poly_in[215:208] ^ { poly_in[235] ^ poly_in[239], poly_in[234] ^ poly_in[238], poly_in[233] ^ poly_in[237], poly_in[232] ^ poly_in[236], poly_in[236] ^ poly_in[237] ^ poly_in[238] ^ poly_in[239], poly_in[237] ^ poly_in[239], poly_in[238], poly_in[238] ^ poly_in[239] };

assign poly_out[119:112] = poly_in[119:112] ^ poly_in[207:200] ^ poly_in[223:216] ^ { poly_in[243] ^ poly_in[247], poly_in[242] ^ poly_in[246], poly_in[241] ^ poly_in[245], poly_in[240] ^ poly_in[244], poly_in[244] ^ poly_in[245] ^ poly_in[246] ^ poly_in[247], poly_in[245] ^ poly_in[247], poly_in[246], poly_in[246] ^ poly_in[247] };

assign poly_out[127:120] = poly_in[127:120] ^ poly_in[215:208] ^ poly_in[231:224] ^ { poly_in[251] ^ poly_in[255], poly_in[250] ^ poly_in[254], poly_in[249] ^ poly_in[253], poly_in[248] ^ poly_in[252], poly_in[252] ^ poly_in[253] ^ poly_in[254] ^ poly_in[255], poly_in[253] ^ poly_in[255], poly_in[254], poly_in[254] ^ poly_in[255] };

endmodule