`timescale 1ns/100ps
`include "aes_gcm.v"

module aes_gcm_tb;

reg						Clk = 0;
reg						Rst;
reg						Enable;
reg			[127:0]		K;
reg			[95:0]		Iv;
reg			[127:0]		P;
reg			[127:0]		A;
reg			[127:0]		Len;

wire					valid;
wire					ready;
wire		[127:0]		c;
wire		[127:0]		t;

integer					i;

always #1 Clk <= ~Clk;

initial begin
	$dumpfile("aes_gcm.vcd");
	$dumpvars;
	#2;
	@ (posedge Clk) begin
		#0.1;
		Rst = 1;
		K = 0;
		Iv = 0;
		A = 0;
		Len = 128'h780;
	end
	@ (posedge Clk) begin
		#0.1;
		Rst = 0;
	end
	wait(ready);
	#2;
	for (i=0; i<15; i=i+1) begin
		@ (posedge Clk) begin
			#0.1;
			Enable = 1;
			P = i;
		end
	end
	@ (posedge Clk) begin
		#0.1;
		Enable = 0;
	end
	wait(ready);
	$display("C0 = %h", AES_GCM_0.C0);
	if (AES_GCM_0.C0 == 128'h58e2fccefa7e3061367f1d57a4e7455a) begin
		$display("Correct.");
	end
	else begin
		$display("Wrong!");
	end
	$display("H = %h (MSB->LSB)", AES_GCM_0.H);
	if (AES_GCM_0.H == 128'hbb4e6f074c557e90b162097d74663775) begin
		$display("Correct.");
	end
	else begin
		$display("Wrong!");
	end
	$display("H13 = %h (MSB->LSB)", AES_GCM_0.H13);
	if (AES_GCM_0.H13 == 128'hbbc251dfeba523e8bbabb22d508f9e8c) begin
		$display("Correct.");
	end
	else begin
		$display("Wrong!");
	end
	#20;
	$display("Authentication Tag = %h", t);
	if (t == 128'h5aadef39f1488f995e407287b0bf7c1f) begin
		$display("Correct.");
	end
	else begin
		$display("Wrong!");
	end
	$finish;
end

aes_gcm AES_GCM_0
	(.clk(Clk), .rst(Rst), .enable(Enable), .k(K), .iv(Iv), .p(P), .a(A), .len(Len), .valid(valid), .ready(ready), .c(c), .t(t));

endmodule
